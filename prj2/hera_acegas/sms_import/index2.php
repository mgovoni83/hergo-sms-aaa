<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
require_once("/server/prj2/hera_acegas/sms_import/_functions.php");

$GLOBALS[SESSION_DEBUG]=0;
start_netbox_session();
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
$debug = 0;
$myDatabase = 'hera_aaa_sms_rifiuti';
$smsImportTable = 'sms_import';
$retCode = 'OK';
unset($_REQUEST);

if(!empty($_POST)){
    $cellulare = $_POST['sender'];
    $messaggio = $_POST['text'];
    $token = hash('md5', microtime());
    $campi_da_convertire = array('text' => 'messaggio',
                                 'sender' => 'CELLULARE');
    foreach($campi_da_convertire as $k => $v)
        $_REQUEST[$v] = iconv('UTF-8', 'ISO-8859-1', ''.$_POST[$k]);
    $_REQUEST['system_status'] = 0;
    $_REQUEST['token'] = $token;
    $params = array(database => $myDatabase, table => $smsImportTable, debug => 0);
    $params['result'] = 'newref';
    $params['type'] = "add";
    $_SESSION['autorizzazione_salvataggio'] = time()-60;
    $retval = control_form_handler($params);
    if(!$newref)
        mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','Riscontrato problema su servizio sms_import: '. $cellulare .' - (sms_import)');
    else
        recordAction($newref, 'Ricezione SMS');
}
?>