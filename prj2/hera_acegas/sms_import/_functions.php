<?php
/*
 * Generazione di un CSV da un array
 */
function outputCSV($data) {
    $outputBuffer = fopen("php://output", 'w');
    foreach($data as $val)
        fputcsv($outputBuffer, $val);
    fclose($outputBuffer);
}

/*
 * Controllo la validità del messaggio inviaato dall'utente.
 * L'invio sarà autorizzato solamente se la stringa alfanumerica ripulita ha un
 * numero massimo di 6 caratteri e non è vuota.
 */
function myCheckSMS($string){
    return (strlen(preg_replace('/[^\w]/', '', $string)) > 0 AND strlen(preg_replace('/[^\w]/', '', $string)) < 7) ? true : false;
}

/*
 * Controllo la validità della data comunicata da Siebel rispetto a giorno ed
 * ora corrente.
 * L'invio sarà autorizzato solamente se la data Siebel è successiva ad oggi,
 * oppure se l'ora attuale è inferiore alle 19.
 */
function myCheckDate($date){
    $now = time();
    $siebelDate = strtotime(DateTime::createFromFormat('d/m/Y', $date)->format('d-m-Y'));
    $todayAtSeven = mktime(19,0,0,date('m'),date('d'),date('Y'));
    if($siebelDate > $now){
        if(date('Ymd') == date('Ymd', $siebelDate)){
            if($siebelDate > $todayAtSeven){
                return false;
            }
            else return true;
        }
        else return true;
    }
    else return false;
}

function getNotified(){
    $database = "hera_aaa_sms_rifiuti";
    $smsSettingsTable = "sms_settings";
    $queryEmail = "SELECT value FROM $database.$smsSettingsTable WHERE config = 'email_notification'";
    $resultEmail = query(array(DBH => $udbh, sql => $queryEmail, direct => 1, debug => $debug, status => 1));
    return $resultEmail[0]['value'];
}

function sendPIRecoveryMail(){
    $emails = explode(',',getNotified());
    foreach($emails as $email)
        mail($email,'Avviso HERGO SMS','OK PI');
    return;
}

function sendPIBreakMail(){
    $emails = explode(',',getNotified());
    foreach($emails as $email)
        mail($email,'Avviso HERGO SMS','KO PI');
    return;
}

function sendAgendaBreakMail($cellulare){
    $emails = explode(',',getNotified());
    $msg = $cellulare === FALSE ? 'KO AGENDA' : 'KO AGENDA - Inviato messaggio di cortesia a '.$cellulare;
    foreach($emails as $email)
        mail($email,'Avviso HERGO SMS',$msg);
    return;
}

function sendSMSBreakMail($cellulare){
    $emails = explode(',',getNotified());
    $msg = $cellulare === FALSE ? 'KO SMS' : 'KO SMS - Inviato messaggio di cortesia a '.$cellulare;
    foreach($emails as $email)
        mail($email,'Avviso HERGO SMS',$msg);
    return;
}

function generateSiebelStatus($ping){
    $database = "hera_aaa_sms_rifiuti";
    $smsPingSapTable = "sms_ping_sap";
    $queryLastStatus = "SELECT system_status FROM $database.$smsPingSapTable ORDER BY REF DESC LIMIT 1";
    $resultLastStatus = query(array(DBH => $udbh, sql => $queryLastStatus, direct => 1, debug => $debug, status => 1));
    $lastStatus = $resultLastStatus[0]['system_status'];
    if($ping){  # ping = 1
        if(!$lastStatus){   # invio mail di ripresa servizio
            sendPIRecoveryMail();
        }
        return true;    # setto lo status a 1
    }
    else{   # ping = 0
        if($lastStatus){    # se l'ultimo status è 1
            if($qCount = getSiebelStatus(false)){   # se lo status corrente è 1
                return true;    # setto lo status a 1
            }
            else{
                sendPIBreakMail();    # invio mail di interruzione servizio
                return false;   # setto lo status a 0
            }
        }
        else{
            return false;   # setto lo status a 0
        }
    }
}

function getSiebelStatus($full = true){
    $database = "hera_aaa_sms_rifiuti";
    $smsPingSapTable = "sms_ping_sap";
    $limit = $full ? 7 : 6;
    $queryCounter = "SELECT COUNT(*) AS ping_counter FROM (".
                        "SELECT ping FROM $database.$smsPingSapTable ORDER BY REF DESC LIMIT $limit".
                    ") AS t WHERE t.ping = 1";
    $resultCounter = query(array(DBH => $udbh, sql => $queryCounter, direct => 1, debug => $debug, status => 1));
    return ($resultCounter[0]['ping_counter'] > 0) ? true : false;
}

/*
 *  Invio messaggio all'utente tramite cron che agisce sulla tabella SMS
 */
function sendCourtesyMsg($cellulare, $msg, $agenda = true){
    $cellulare = str_replace('+39','',$cellulare);
    $database = 'hera_aaa_sms_rifiuti';
    $smsTable = "sms";
    unset($_REQUEST);
    $_REQUEST['ipaddress'] = $_SERVER['REMOTE_ADDR'];
    $_REQUEST['system_status'] = 1;
    $_REQUEST['messaggio'] = $msg;
    $_REQUEST['data'] = date('Y-m-d H:i:s');
    $_REQUEST['CELLULARE'] = iconv('UTF-8', 'ISO-8859-1', $cellulare);
    $_REQUEST['token'] = iconv('UTF-8', 'ISO-8859-1', '1');
    $_REQUEST['idrichiedente'] = iconv('UTF-8', 'ISO-8859-1', 1);

    $params = array(database => $database, table => $smsTable, debug => 0);
    $params['result'] = 'newref';
    $params['type'] = "add";
    $_SESSION['autorizzazione_salvataggio'] = time()-60;
    if($retval = control_form_handler($params)){    # scrivo i dati su DB
        if($agenda)
            sendAgendaBreakMail($cellulare);
        else
            sendSMSBreakMail($cellulare);
    }
    else{
        if($agenda)
            sendAgendaBreakMail(false);
        else
            sendSMSBreakMail(false);
    }
    return true;
}

function get_url($url, $postData, $timeout = 30){
    $headers = array("Content-type: text/xml;charset=\"utf-8\"",
                     "Accept: text/xml",
                     "Cache-Control: no-cache",
                     "Pragma: no-cache",
                     "SOAPAction: 'http://sap.com/xi/WebService/soap1.1'");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt($ch, CURLOPT_ENCODING, "" );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_AUTOREFERER, true );
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    $content = curl_exec($ch);
    $response = curl_getinfo($ch);
    curl_close($ch);
    return ($response['http_code'] != 200) ? false : $content;
}

/*
 * Creazione record di bypass sulla tabella sms_bypass_sap: al verificarsi di
 * determinate condizioni il messaggio non viene spedito a Siebel e l'utente
 * riceve un SMS di cortesia. Tutto quanto viene impostato via BO da HERA AAA.
 */
function bypassSAP($cellulare, $smsImportRef, $bypassRef, $message){
    $cellulare = str_replace('+39','',$cellulare);
    $database = 'hera_aaa_sms_rifiuti';
    $smsImportTable = "sms_import";
    $smsTable = 'sms';
    unset($_REQUEST);
    $_REQUEST['ipaddress'] = $_SERVER['REMOTE_ADDR'];
    $_REQUEST['system_status'] = 1;
    $_REQUEST['messaggio'] = $message;
    $_REQUEST['data'] = date('Y-m-d H:i:s');
    $_REQUEST['CELLULARE'] = iconv('UTF-8', 'ISO-8859-1', $cellulare);
    $_REQUEST['token'] = iconv('UTF-8', 'ISO-8859-1', '0');
    $_REQUEST['idrichiedente'] = iconv('UTF-8', 'ISO-8859-1', 1);
    $_REQUEST['idbypass'] = iconv('UTF-8', 'ISO-8859-1', $bypassRef);

    $params = array(database => $database, table => $smsTable, debug => 0);
    $params['result'] = 'newref';
    $params['type'] = "add";
    $_SESSION['autorizzazione_salvataggio'] = time()-60;
    $retval = control_form_handler($params);	# scrivo i dati su DB
    return ($retval AND dbdo(array(QUERY_1 => "UPDATE $database.$smsImportTable SET system_modified = '". date("Y-m-d H:i:s") ."', system_status = 0, checked = 0, queued = 1 WHERE REF = $smsImportRef", DBH => $GLOBALS['DBH'])) AND recordAction($smsImportRef, 'QUEUED (BYPASS '. $bypassRef.')')) ? true : false;
}

/*
 * Registro le azioni relative alla tabella sms_import.
 */
function recordAction($smsImportRef, $operation){
    $database = 'hera_aaa_sms_rifiuti';
    $smsImportEventTable = "sms_import_event";
    unset($_REQUEST);
    $_REQUEST['sms_import_ref'] = $smsImportRef;
    $_REQUEST['operation'] = $operation;
    $params = array(database => $database, table => $smsImportEventTable, debug => 0);
    $params['result'] = 'newref';
    $params['type'] = "add";
    $_SESSION['autorizzazione_salvataggio'] = time()-60;
    $retval = control_form_handler($params);
    return $retval;
}