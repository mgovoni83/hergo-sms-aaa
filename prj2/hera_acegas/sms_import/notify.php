<?php
if(isset($_POST['act']) AND $ref = $_POST['act']){
    $codici_lingua = array("it" => 1, "en" => 2);
    require_once("pwd/SUPER_USER_CONNECT.php");
    require_once("pwd/Hera.php");
    require_once("super_connect.php");
    require_once("sms/lib-mobytsms.inc.php");
    if(!class_exists('soapclient'))
        require_once("sms/lib-nusoap.inc.php");
    require_once("adm/sms_tracker.php");
    require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

    $database = 'hera_aaa_sms_rifiuti';
    $table = 'sms';

    unset($_REQUEST);
    $_REQUEST['mobyt_status'] = iconv('UTF-8', 'ISO-8859-1', $_POST['status']);
    $exploded_date = explode('-',$_POST['date']);
    $date = implode('-',array_reverse($exploded_date)) .' '. $_POST['time'];
    $_REQUEST['data'] = iconv('UTF-8', 'ISO-8859-1', $date);
    $params = array(database => $database, table => $table, debug => 0);
    $params['ref'] = $ref;
    $params['type'] = "edit";
    $_SESSION['autorizzazione_salvataggio'] = time()-60;
    $retval = control_form_handler($params);    # scrivo i dati su DB
}
?>