jQuery(function($){
//  Strumento di pulizia rows textarea di exclusion list
    function cleanTextarea(){
        var exclist = $('#exclusionList').val();
        $('#exclusionList').val('');
        rows = exclist.split("\n");
        counter = 0;
        rowCount = rows.length;
        for(i in rows){
            var row = rows[i].trim().replace(/[^0-9]+/g, '');
            counter++;
            if(row != ''){
                $('#exclusionList').val($('#exclusionList').val() + row);
                if(counter < rowCount)
                    $('#exclusionList').val($('#exclusionList').val() + '\n');
            }
        }
    }
//  Validazione campo email
    function validateEmail(email){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    
//  Reset form creazione blocco
    function resetBlockModal(){
        $("#new-block-modal form").find(':input').not('.start, .stop').each(function() {
            switch(this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'hidden':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });
    }

//  Salvataggio form blocco
    function saveBlock(){
        var messagefield = $("#new-block-modal input[name=message]");
        var startfield = $("#new-block-modal input[name=start]");
        var stopfield = $("#new-block-modal input[name=stop]");
        if (messagefield.val() == ""){
            messagefield.removeClass('success').addClass('error');
            $("#new-block-modal #output").removeClass('alert alert-success').addClass("alert alert-danger animated fadeInUp").html("Devi inserire il messaggio");
        }
        else if(startfield.val() == "") {
            startfield.removeClass('success').addClass('error');
            $("#new-block-modal #output").removeClass('alert alert-success').addClass("alert alert-danger animated fadeInUp").html("Devi inserire data/ora iniziale");
        }
        else if(stopfield.val() == "") {
            stopfield.removeClass('success').addClass('error');
            $("#new-block-modal #output").removeClass('alert alert-success').addClass("alert alert-danger animated fadeInUp").html("Devi inserire data/ora finale");
        }
        else {
            cleanTextarea();
            var stopDateSplit1 = $(stopfield).val().split(" ");
            var stopDateSplit2 = stopDateSplit1[0].split("/");
            var stopDate = new Date(stopDateSplit2[1] +'/'+ stopDateSplit2[0] +'/'+ stopDateSplit2[2] +' '+ stopDateSplit1[1]).getTime() / 1000;
            var startDateSplit1 = $(startfield).val().split(" ");
            var startDateSplit2 = startDateSplit1[0].split("/");
            var startDate = new Date(startDateSplit2[1] +'/'+ startDateSplit2[0] +'/'+ startDateSplit2[2] +' '+ startDateSplit1[1]).getTime() / 1000;
            var ref = (($('#new-block-modal button[type="submit"]').attr('data-type') == 'add') ? 0 : $("#new-block-modal input[name=ref]").val());
            var exclusionList = $("#new-block-modal textarea[name=exclusionList]").val();
            $.post('ajax2.php', {ref: ref, message: messagefield.val(), start: startDate, stop: stopDate, exclusionList: exclusionList})
            .done(function(data){
                result = JSON.parse(data);
                if(parseInt(result.status))
                    location.reload();
                else{
                    $("#new-block-modal #output").removeClass('alert alert-success');
                    $("#new-block-modal #output").addClass("alert alert-danger animated fadeInUp").html(result.message);
                }
            })
            .fail(function(){
                $("#new-block-modal #output").removeClass('alert alert-success').addClass("alert alert-danger animated fadeInUp").html('Problema di connessione al database, Riprova pi&ugrave; tardi!');
            });
        }
    }

//  Hash tab riportati nella URL per mantenere coerenza di navigazione
    function initialize(){
        $.fn.dataTable.moment('DD/MM/YYYY HH:mm');
        /* Dynamic form field creation */
        $(document).on('click', '#mainmanagement .btn-add', function(e){
            e.preventDefault();
            var email = $(this).parent().siblings('input').val();
            if(validateEmail(email)){
                $(this).parents('.entryContainer:first').removeClass('new-entry');
                $(this).parent().siblings('.alert').addClass('sr-only');
                $(this).parent().parent().removeClass('has-error');
                var controlForm = $(this).parents('.controls .entry'),
                    currentEntry = $(this).parents('.entryContainer:first'),
                    newEntry = $(currentEntry.clone()).appendTo(controlForm);
                newEntry.find('input').val('');
                controlForm.find('.entryContainer:not(:last) .btn-add')
                           .removeClass('btn-add').addClass('btn-remove')
                           .removeClass('btn-success').addClass('btn-danger')
                           .html('<span class="glyphicon glyphicon-minus"></span>');
            }
            else{
                if($(this).parent().parent().hasClass('has-error')){
                    
                }
                else{
                    var alertEntry = $('#mainmanagement > .alert'),
                        newAlert = $(alertEntry.clone()).appendTo($(this).parent().parent());
                    $(this).parent().parent().addClass('has-error');
                    newAlert.removeClass('sr-only');
                }
            }
        }).on('click', '#mainmanagement .btn-remove', function(e){
            $(this).parents('.entryContainer:first').remove();
            e.preventDefault();
            return false;
        });
        $(document).on('blur', '#mainmanagement .entryContainer input.form-control', function(e){
            e.preventDefault();
            var email = $(this).val();
            if(($(this).parent().hasClass('new-entry') && email == '') || validateEmail(email)){
                $(this).parent().removeClass('has-error');
                $(this).siblings('.alert').addClass('sr-only');
            }
            else{
                if($(this).parent().hasClass('has-error')){
                    
                }
                else{
                    var alertEntry = $('#mainmanagement > .alert'),
                        newAlert = $(alertEntry.clone()).appendTo($(this).parent());
                    $(this).parent().addClass('has-error');
                    newAlert.removeClass('sr-only');
                }
            }
        });
        $(document).on('submit', '#mainmanagement form', function(e){
            e.preventDefault();
            if($('#mainmanagement .alert').not('.sr-only').length > 0){}
            else{
                $.post("ajax6.php", $(this).serialize()).done(function(data){
                    result = JSON.parse(data);
                    if(parseInt(result.status))
                        $("#save-cfg-modal").modal({backdrop:'static', keyboard:false});
                });
            }
        });
        /* File Upload */
        var geolocButton = '<span class="btn-group" role="group" aria-label="actions"><button title="Importa dati" type="button" class="btn btn-success btn-lg btn-geoloc-import"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Importa</button></span>';
        $('#fileupload').fileupload({
            url: 'ajax8.php',
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p><strong>' + file.name + ' - ' + niceBytes(file.size) + '</strong> ' + geolocButton + '</p>').appendTo('#files');
                    $('.btn-geoloc-import').click(function(){
                        $(this).children().removeClass('glyphicon-ok').addClass('glyphicon-refresh glyphicon-spin');
                        $.post("ajax10.php", {filename: file.name})
                        .done(function(data){
                            result = JSON.parse(data);
                            console.log(result);
                            if(parseInt(result.status))
                                redirectPost(window.location.pathname+'#geolocmanagement', result.message);
                            else
                                alert(result.message.error);
                        })
                        .fail(function(){
                            alert('Problema di importazione, si prega di contattare il gestore del sito!');
                            $(this).removeClass('glyphicon-refresh glyphicon-spin').addClass('glyphicon-ok');
                        });                        
                    });
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css('width', progress + '%');
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
        /* Mantenimento del tab corrente al reload */
        var url = document.location.toString();
        if (url.match('#'))
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        });
        $('#dataTable tr td button.btn-bypass-download').click(function(){
            var tr = $(this).closest('tr');
            if(!tr.hasClass('incoming')){
                var data = table.row(tr).data();
                location.href = '/sms_import/report/report2.php?ref='+ data[0];
            }
        });
        $('#dataTable tr td button.btn-bypass-remove').click(function(){
            $(this).children().removeClass('glyphicon-remove').addClass('glyphicon-refresh glyphicon-spin');
            var ref = table.row($(this).closest('tr')).data();
            $.post("ajax3.php", {ref: ref[0]})
            .done(function(data){
                result = JSON.parse(data);
                if(parseInt(result.status))
                    location.reload();
                else
                    alert(result.message);
            })
            .fail(function(){
                alert('Problema di connessione al database, Riprova pi&ugrave; tardi!');
                $(this).removeClass('glyphicon-refresh glyphicon-spin').addClass('glyphicon-remove');
            });
        });
        $('#new-block-modal button.closeModal').click(function() {
            $('#new-block-modal').modal('hide');
        });
        $('#new-block-modal button[type="submit"]').click(function(e) {
            e.preventDefault();
            saveBlock();
        });
        $('#dataTable tr td button.btn-bypass-edit').click(function(){
            var row = table.row($(this).closest('tr')).data();
            $.post("ajax4.php", {ref: row[0]})
            .done(function(data){
                result = JSON.parse(data);
                $("#new-block-modal").modal({backdrop:'static', keyboard:false});
                $("#new-block-modal input[name=message]").val(result[0].message);
                $("#new-block-modal input[name=ref]").val(result[0].REF);
                $("#new-block-modal textarea[name=exclusionList]").val(result[0].exclusion_list);
                $('#datetimepicker6').datetimepicker({locale:moment().local('it')});
                $('#datetimepicker7').datetimepicker({locale:moment().local('it')});
                $('#datetimepicker6').data("DateTimePicker").date(new Date(parseInt(result[0].start)*1000));
                $('#datetimepicker7').data("DateTimePicker").date(new Date(parseInt(result[0].stop)*1000));
                $("#datetimepicker6").on("dp.change", function (e) {
                    $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                });
                $('#new-block-modal button[type="submit"]').attr('data-type', 'edit');
            });
        });
        $('#geolocTable tr td button.btn-geoloc-remove').click(function(){
            $(this).children().removeClass('glyphicon-remove').addClass('glyphicon-refresh glyphicon-spin');
            var ref = $(this).closest('tr').attr('data-ref');
            $.post("ajax7.php", {ref: ref})
            .done(function(data){
                result = JSON.parse(data);
                if(parseInt(result.status))
                    location.reload();
                else
                    alert(result.message);
            })
            .fail(function(){
                alert('Problema di connessione al database, Riprova pi&ugrave; tardi!');
                $(this).removeClass('glyphicon-refresh glyphicon-spin').addClass('glyphicon-remove');
            });
        });
        $('.logout').click(function(e){
            e.preventDefault();
            location.replace("logout.php");
        });
        //  Creazione dataTable per griglia blocchi SAP
        var table = $('#dataTable').DataTable({
            dom: 'Bfrtip',
            buttons: {
                buttons: [
                    {text: '<span class="glyphicon glyphicon-plus-sign btn-new-block" aria-hidden="true"></span> Crea blocco',
                     className: 'btn btn-primary btn-lg',
                     tag: 'span',
                    //  Apertura modal di creazione nuovo blocco SAP
                     action: function(e,dt,node,config){
                        resetBlockModal();
                        $("#new-block-modal").modal({backdrop:'static', keyboard:false});
                        $('#datetimepicker6').datetimepicker({locale:moment().local('it'), minDate:moment(), defaultDate:moment()});
                        $('#datetimepicker7').datetimepicker({locale:moment().local('it'), minDate:moment(), defaultDate:moment()});
                        $("#datetimepicker6").on("dp.change", function (e){
                            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                        });
                     }
                    },
                ]
            },
            order: [[2,"desc"]],
            language: {
                "decimal": "",
                "emptyTable": "Nessun dato disponibile",
                "info": "Risultati da _START_ a _END_ di _TOTAL_",
                "infoEmpty": "Non ci sono risultati",
                "infoFiltered": "filtrato dal totale risultati (_MAX_)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Visualizza _MENU_ risultati",
                "loadingRecords": "Caricamento...",
                "processing": "In lavorazione...",
                "search": "_INPUT_",
                "searchPlaceholder": "Ricerca...",
                "zeroRecords": "Nessun risultato disponibile",
                "paginate": {
                    "first": "Inizio",
                    "last": "Fine",
                    "next": "Successiva",
                    "previous": "Precedente"
                },
            }
        });
        var queueTable = $('#queueTable').DataTable({
            "dom": 'Blftipr',
            "buttons": {
                buttons: [
                    {extend: 'selectAll',
                     tag: 'span',
                     className: 'btn btn-primary btn-lg',
                     text: '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Seleziona tutto',
                    },
                    {text: '<span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Seleziona visibili',
                        className: 'btn btn-primary btn-lg',
                        tag: 'span',
                        action: function(e,dt,node,config){
                           queueTable.rows({search: 'applied'}).select();
                        }
                    },
                    {extend: 'selectNone',
                     tag: 'span',
                     className: 'btn btn-primary btn-lg',
                     text: '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Reset selezione',
                    },
                    {text: '<span class="glyphicon glyphicon-share" aria-hidden="true"></span> Rimetti in coda',
                        className: 'btn btn-warning btn-lg',
                        tag: 'span', 
                        action: function(e,dt,node,config){
                           if(queueTable.rows('.selected').data().length > 0){
                               $('#confirm-modal').modal({
                                   backdrop: 'static',
                                   keyboard: false
                               })
                               .one('click', '.confirm', function(e) {
                                   var refs = {};
                                   queueTable.rows('.selected').every(function(){
                                       data = this.data();
                                       refs[data[9]] = data[9];
                                   });
                                   $.post('ajax5.php', {refs: refs, mode: 'resend'})
                                   .done(function(data){
                                       result = JSON.parse(data);
                                       if(parseInt(result.status))
                                           location.reload();
                                       else
                                           alert('Error!');
                                   });
                               });
                           } else {
                               $('#error-modal').modal({
                                   backdrop: 'static',
                                   keyboard: false
                               });
                           }
                        }
                    },
                    {text: '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Segna come risolto',
                        className: 'btn btn-success btn-lg',
                        tag: 'span',
                        action: function(e,dt,node,config){
                            if(queueTable.rows('.selected').data().length > 0){
                                $('#confirm-check-modal').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                })
                                .one('click', '.confirm-check', function(e) {
                                    var refs = {};
                                    queueTable.rows('.selected').every(function(){
                                        data = this.data();
                                        refs[data[9]] = data[9];
                                    });
                                    $.post('ajax5.php', {refs: refs, mode: 'check'})
                                    .done(function(data){
                                        result = JSON.parse(data);
                                        if(parseInt(result.status))
                                            location.reload();
                                        else
                                            alert('Error!');
                                    });
                                });
                            } else {
                                $('#error-modal').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                            }
                        }
                    },
                    {text: '<span class="glyphicon glyphicon-download" aria-hidden="true"></span> Genera report',
                        className: 'btn btn-default btn-lg',
                        tag: 'span',
                        action: function(e,dt,node,config){
                            queueReport(queueTable);
                        }
                    }
                ],
            },
            "autoWidth": false,
            "columns": [
                {"className": "select-checkbox", "width": "30px", "orderable": false},
                {"className": "text-center", "width": "75px"},
                {"width": "50px"},
                {"width": "250px"},
                {"className": "text-center", "width": "30px"},
                {"width": "200px"},
                {"width": "75px"},
                {"width": "75px"},
                {"width": "200px", "orderable": false}
            ],
            "columnDefs": [
                {
                    "targets": [ 9 ],
                    "visible": false,
                    "searchable": false
                },
            ],
            "select": {
                style: 'multi',
                selector: 'td:first-child'
            },
            "order": [[ 2, 'desc' ]],
            "language": {
                "decimal": "",
                "emptyTable": "Nessun dato disponibile",
                "info": "Risultati da _START_ a _END_ di _TOTAL_",
                "infoEmpty": "Non ci sono risultati",
                "infoFiltered": "filtrato dal totale risultati (_MAX_)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Visualizza _MENU_ risultati",
                "loadingRecords": "Caricamento...",
                "processing": "In lavorazione...",
                "search": "_INPUT_",
                "searchPlaceholder": "Ricerca...",
                select: {
                    rows: "%d elemento/i selezionati",
                },
                "searchPlaceholder": "Ricerca...",
                "zeroRecords": "Nessun risultato disponibile",
                buttons: {
                    selectAll: "Seleziona tutto",
                    selectNone: "Deseleziona tutto"
                },
                "paginate": {
                    "first": "Inizio",
                    "last": "Fine",
                    "next": "Successiva",
                    "previous": "Precedente"
                },
            }
        });/*
        queueTable.columns().every(function(){
            var column = this;
            $('input', this.footer()).on('keyup change', function(){
                column.search(this.value).draw();
            });
        });*/
        $('#geolocTable tfoot th').each(function(){
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="'+title+'" />');
        });
        var countsmsTable = $('#countsmsTable').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                };
                // Total over all pages
                total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    /*
                // Total over this page
                pageTotal = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );*/
                // Update footer
                $(api.column(4).footer()).html(total);
            }
        });
        var geolocTable = $('#geolocTable').DataTable({
            order: [[0, 'desc']],
            "language": {
                "decimal": "",
                "emptyTable": "Nessun dato disponibile",
                "info": "Risultati da _START_ a _END_ di _TOTAL_",
                "infoEmpty": "Non ci sono risultati",
                "infoFiltered": "filtrato dal totale risultati (_MAX_)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Visualizza _MENU_ risultati",
                "loadingRecords": "Caricamento...",
                "processing": "In lavorazione...",
                "search": "_INPUT_",
                "searchPlaceholder": "Ricerca...",
                "zeroRecords": "Nessun risultato disponibile",
                "paginate": {
                    "first": "Inizio",
                    "last": "Fine",
                    "next": "Successiva",
                    "previous": "Precedente"
                },
            }
        });
        geolocTable.columns().every(function(){
            var that = this;
            $('input', this.footer()).on('keyup change', function(){
                if(that.search() !== this.value)
                    that.search(this.value).draw();
            });
        });
        $('.dataTables_filter input').wrap('<div class="inner-addon left-addon"></div>');
        $('.dataTables_filter .inner-addon').prepend('<i class="glyphicon glyphicon-user"></i>');
        $('#queueTable .btn-group button.btn-queue-report').click(function(){
            location.href = '/sms_import/report/report3.php?refs='+ $(this).closest('tr').attr('data-ref');
        });
        $('#queueTable').on('click', 'button.btn-queue-check,button.btn-queue-resend', function(){
            var ref = $(this).closest('tr').attr("data-ref");
            var refs = {ref: ref};
            if($(this).hasClass('btn-queue-check')){
                var mode = 'check';
                var mymodal = '#confirm-check-modal';
            }
            else{
                var mode = 'resend';
                var mymodal = '#confirm-modal';
            }
            $(mymodal).modal({
                backdrop: 'static',
                keyboard: false
            })
            .one('click', '.confirm,.confirm-check', function(e) {
                $.post('ajax5.php', {refs: refs, mode: mode})
                .done(function(data){
                    result = JSON.parse(data);
                    if(parseInt(result.status))
                        location.reload();
                    else
                        alert('Error!');
                });
            });
        });
        //  Verifica e pulizia lista numeri di cellulare
        $('.btn-check-textarea').click(function(e){
            cleanTextarea();
        });
    }
    initialize();
});

function queueReport(queueTable){
    if(queueTable.rows('.selected').data().length > 0){
        var refs = '';
        queueTable.rows('.selected').every(function(){
            data = this.data();
            refs += (refs == '') ? data[9] : ',' + data[9];
        });
        location.href = '/sms_import/report/report3.php?refs='+ refs;
    } else {
        $('#error-modal').modal({
            backdrop: 'static',
            keyboard: false
        });
    }
}

const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
function niceBytes(x){
    let l = 0, n = parseInt(x, 10) || 0;
    while(n >= 1024 && ++l)
        n = n/1024;
    return(n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
}
// Funzione per fare redirect con POST
function redirectPost(path, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", path);
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}