<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
require_once("/server/prj2/hera_acegas/sms_import/_functions.php");

$database = 'hera_aaa_sms_rifiuti';
$table = 'sms_import';
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
$debug = 0;
unset($_REQUEST);
$refs = implode(',', $_POST['refs']);
$systemModified = date("Y-m-d H:i:s");
# check = già risposto al cliente / resend = rimesso nella lista degli invii
$systemStatus = $_POST['mode'] == 'check' ? 1 : 0;
$checked = $_POST['mode'] == 'check' ? 1 : 0;
$operation = $_POST['mode'] == 'check' ? 'OK da BO' : 'Reinvio da BO';
$status = dbdo(array(QUERY_1 => "UPDATE $database.$table SET system_modified = '$systemModified', checked = $checked, queued = 0, system_status = $systemStatus WHERE REF IN ($refs)", DBH => $GLOBALS['DBH']));
foreach($_POST['refs'] as $smsImportRef)
    recordAction($smsImportRef, $operation);
$output = array('status' => $status);
echo json_encode($output);
?>