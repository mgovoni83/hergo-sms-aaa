<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
$login = $_SESSION['login'];
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
$GLOBALS[SESSION_DEBUG]=0;
start_netbox_session();
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
$debug = 0;
$smsTable = 'sms';
$smsbypasssapTable = 'sms_bypass_sap';
$smsrichiedentiTable = 'sms_richiedenti';
$smsimportTable = 'sms_import';
$smsimporteventTable = 'sms_import_event';
$smsgeolocTable = 'sms_geoloc';
$smssettingsTable = 'sms_settings';
unset($_REQUEST);
$database = "hera_aaa_sms_rifiuti";
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script type="text/javascript">var login = '<?php echo $login ?>';</script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Reportistica SAP Bypass</title>
        <!-- Bootstrap -->
        <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/dataTables/datatables.min.css" rel="stylesheet">
        <link href="../assets/jQueryFileUpload/css/style.css" rel="stylesheet">
        <link href="../assets/jQueryFileUpload/css/jquery.fileupload.css" rel="stylesheet">
        <link href="../assets/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href="../assets/od/css/od.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
<!-- BEGIN # MODAL CONFIRM -->
        <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Conferma operazione</h4>
                    </div>
                    <div class="modal-body">
                        <p>I messaggi selezionati saranno inviati nuovamente alla coda di elaborazione. Confermi?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary confirm" type="submit">Ok</button>
                        <button class="btn btn-primary cancel" type="submit" data-dismiss="modal">Annulla</button>
                    </div>
                </div>
            </div>
        </div>
<!-- END # MODAL CONFIRM -->
<!-- BEGIN # MODAL CONFIRM CHECK -->
        <div class="modal fade" id="confirm-check-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Conferma operazione</h4>
                    </div>
                    <div class="modal-body">
                        <p>I messaggi selezionati verranno eliminati dalla coda e considerati in stato completato.</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary confirm-check" type="submit">Ok</button>
                        <button class="btn btn-primary cancel" type="submit" data-dismiss="modal">Annulla</button>
                    </div>
                </div>
            </div>
        </div>
<!-- END # MODAL CONFIRM -->
<!-- BEGIN # MODAL ERROR -->
        <div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Notifica d'errore</h4>
                    </div>
                    <div class="modal-body">
                        <p>Devi selezionare almeno un elemento.</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default cancel" type="submit" data-dismiss="modal">Chiudi</button>
                    </div>
                </div>
            </div>
        </div>
<!-- END # MODAL ERROR -->
<!-- BEGIN # MODAL SAVE CFG -->
        <div class="modal fade" id="save-cfg-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Salvataggio completato</h4>
                    </div>
                    <div class="modal-body">
                        <p>La nuova configurazione &egrave; stata correttamente salvata.</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default cancel" type="submit" data-dismiss="modal">Chiudi</button>
                    </div>
                </div>
            </div>
        </div>
<!-- END # MODAL SAVE CFG -->
<!-- BEGIN # MODAL NEW BLOCK -->
        <div class="modal fade" id="new-block-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-new-block">
                <div class="container new-block-container">
                    <div id="output"></div>
                    <div class="form-box">
                        <form method="POST">
                            <input name="ref" type="hidden" value="0" />
                            <div class="row">
                                <div class='col-xs-10 col-xs-offset-1'>
                                    <h3>Propriet&agrave; del blocco</h3>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class='col-xs-10 col-xs-offset-1'>
                                    <input name="message" type="text" placeholder="messaggio all'utente" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class='col-md-4 col-md-offset-1 col-xs-10 col-xs-offset-1'>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker6'>
                                            <input name="start" type='text' class="form-control start" placeholder="Data/Ora iniziale" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-4 col-md-offset-2 col-xs-10 col-xs-offset-1'>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker7'>
                                            <input name="stop" type='text' class="form-control stop" placeholder="Data/Ora finale" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class='col-xs-8 col-xs-offset-1'>
                                    <textarea id="exclusionList" class="form-control" name="exclusionList" rows="5" placeholder="elenco numeri di telefono (un numero per riga)"></textarea>
                                </div>
                                <div class='col-xs-2'>
                                    <button class="btn btn-warning btn-check-textarea" type="button"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Verifica e pulizia dati</button>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class='col-xs-6 col-xs-offset-3 text-center'>
                                    <div class="btn-group" role="group" aria-label="actions">
                                        <button class="btn btn-success btn-save-new-block" type="submit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Salva</button>
                                        <button class="btn btn-default closeModal" type="button"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Annulla</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<!-- END # MODAL NEW BLOCK -->
        <div class="main-container container<?php if(!$login) echo ' hidden' ?>">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs nav-justified lazyload">
                        <?php
                            $queryActions = sprintf("SELECT actions FROM %s WHERE REF = %d ORDER BY REF ASC",$database.'.'.$smsrichiedentiTable,$login);
                            $actionList = query(array(DBH => $udbh, sql => $queryActions, direct => 1, debug => $debug, status => 1));
                            $actions = explode(',',$actionList[0]['actions']);
                            if(in_array('blockmanagement',$actions))
                                echo '<li class="active blockmanagement"><a data-toggle="tab" href="#blockmanagement"><span><span class="glyphicon glyphicon-ban-circle"></span> Gestione Blocco SMS</span></a></li>';
                            if(in_array('queuemanagement',$actions))
                                echo '<li class="queuemanagement"><a data-toggle="tab" href="#queuemanagement"><span><span class="glyphicon glyphicon-tasks"></span> Gestione Coda SMS</span></a></li>';
                            if(in_array('geolocmanagement',$actions))
                                echo '<li class="geolocmanagement"><a data-toggle="tab" href="#geolocmanagement"><span><span class="glyphicon glyphicon-map-marker"></span> Geolocalizzazione utenti</span></a></li>';
                            if(in_array('countsmsmanagement',$actions))
                                echo '<li class="countsmsmanagement"><a data-toggle="tab" href="#countsmsmanagement"><span><span class="glyphicon glyphicon-dashboard"></span> Counter SMS</span></a></li>';
                            if(in_array('mainmanagement',$actions))
                                echo '<li class="mainmanagement"><a data-toggle="tab" href="#mainmanagement"><span><span class="glyphicon glyphicon-cog"></span> Impostazioni generali</span></a></li>';
                        ?>
                        <li class="log-out"><a data-toggle="tab" class="logout" href="#"><span><span class="glyphicon glyphicon-log-out"></span> Log Out</span></a></li>
                    </ul>
                    <div class="tab-content">
                    <?php if(in_array('mainmanagement',$actions)){ ?>
                        <div id="mainmanagement" class="tab-pane fade">
                            <div class="alert alert-danger sr-only" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>Enter a valid email address
                            </div>
                            <br/>
                            <h2>Impostazioni generali</h2>
                            <div class="container">
                                <form role="form" autocomplete="off">
                                    <div class="row">
                                        <div class="control-group col-xs-8" id="settings">
                                            <div>
                                                <?php
                                                    $query = "SELECT * FROM $database.$smssettingsTable WHERE config = 'ko_sms'";
                                                    $KOSMS = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                                                ?>
                                                <label class="control-label">SMS di cortesia per messaggio utente errato</label>
                                                <div class="controls">
                                                    <textarea name="KOSMS" class="form-control noResize" rows="5" id="KOSMS"><?php echo $KOSMS[0]['value'] ?></textarea>
                                                </div>
                                            </div>
                                            <br/>
                                            <div>
                                                <?php
                                                    $query = "SELECT * FROM $database.$smssettingsTable WHERE config = 'ko_pi'";
                                                    $KOPI = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                                                ?>
                                                <label class="control-label">SMS di cortesia con problema su PI Siebel</label>
                                                <div class="controls">
                                                    <textarea name="KOPI" class="form-control noResize" rows="5" id="KOPI"><?php echo $KOPI[0]['value'] ?></textarea>
                                                </div>
                                            </div>
                                            <br/>
                                            <div>
                                                <?php
                                                    $query = "SELECT * FROM $database.$smssettingsTable WHERE config = 'ko_agenda'";
                                                    $KOAG = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                                                ?>
                                                <label class="control-label">SMS di cortesia con problema su Agenda Siebel</label>
                                                <div class="controls">
                                                    <textarea name="KOAG" class="form-control noResize" rows="5" id="KOAG"><?php echo $KOAG[0]['value'] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group col-xs-4" id="settings">
                                            <label class="control-label">Destinatari avvisi stato Siebel</label>
                                            <div class="controls">
                                                <div class="entry">
                                                <?php
                                                    $query = "SELECT * FROM $database.$smssettingsTable WHERE config = 'email_notification'";
                                                    $EN = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                                                    foreach(explode(',', $EN[0]['value']) as $receiver){
                                                ?>
                                                    <div class="entryContainer input-group">
                                                        <input class="form-control" name="emails[]" type="text" value="<?php echo $receiver ?>" />
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-danger btn-remove" type="button">
                                                                <span class="glyphicon glyphicon-minus"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                <?php } ?>
                                                    <div class="entryContainer input-group new-entry">
                                                        <input class="form-control" name="emails[]" type="text" placeholder="Indirizzo Email" />
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-primary btn-add" type="button">
                                                                <span class="glyphicon glyphicon-plus"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="divider"></div>
                                        </div>
                                        <div class="col-xs-12 text-center">
                                            <button class="btn btn-default btn-success btn-lg" tabindex="0" aria-controls="queueTable">
                                                <span><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Salva configurazione</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>    
                            </div>
                        </div>
                        <?php } if(in_array('blockmanagement',$actions)){ ?>
                        <div id="blockmanagement" class="tab-pane fade in active">
                            <br/>
                            <h2>Blocco SMS con reportistica</h2>
                            <?php                                
                                $query = "SELECT * FROM $database.$smsbypasssapTable WHERE REF > 14 ORDER BY REF ASC";
                                $bypassList = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                            ?>
                            <table id="dataTable" class="ui celled table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Identificativo</th>
                                        <th>Data Inizio</th>
                                        <th>Data Fine</th>
                                        <th>Messaggio</th>
                                        <th>Link</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Identificativo</th>
                                        <th>Data Inizio</th>
                                        <th>Data Fine</th>
                                        <th>Messaggio</th>
                                        <th>Link</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                foreach($bypassList as $bypass):
                                    $REF = $bypass['REF'];
                                    $start = date('d/m/Y H:i', $bypass['start']);
                                    $stop = date('d/m/Y H:i', $bypass['stop']);
                                    if(time('now') <= $bypass['start']){
                                    ?>
                                        <tr class="incoming">
                                            <td><?php echo $REF ?></td>
                                            <td data-order="<?php echo $bypass['start'] ?>"><?php echo $start ?></td>
                                            <td data-order="<?php echo $bypass['stop'] ?>"><?php echo $stop ?></td>
                                            <td><?php echo $bypass['message'] ?></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="actions">
                                                    <button title="Modifica" type="button" class="btn btn-warning btn-lg btn-bypass-edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
                                                    <button title="Elimina" type="button" class="btn btn-danger btn-lg btn-bypass-remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } elseif(time('now') > $bypass['start'] AND time('now') <= $bypass['stop']) { ?>
                                        <tr class="processing">
                                            <td><?php echo $REF ?></td>
                                            <td data-order="<?php echo $bypass['start'] ?>"><?php echo $start ?></td>
                                            <td data-order="<?php echo $bypass['stop'] ?>"><?php echo $stop ?></td>
                                            <td><?php echo $bypass['message'] ?></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="actions">
                                                    <button title="Scarica report" type="button" class="btn btn-default btn-lg btn-bypass-download"><span class="glyphicon glyphicon-download" aria-hidden="true"></span></button>
                                                    <button title="Modifica" type="button" class="btn btn-warning btn-lg btn-bypass-edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
                                                    <button title="Elimina" type="button" class="btn btn-danger btn-lg btn-bypass-remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } else { ?>
                                        <tr class="completed">
                                            <td><?php echo $REF ?></td>
                                            <td data-order="<?php echo $bypass['start'] ?>"><?php echo $start ?></td>
                                            <td data-order="<?php echo $bypass['stop'] ?>"><?php echo $stop ?></td>
                                            <td><?php echo $bypass['message'] ?></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="actions">
                                                    <button title="Scarica report" type="button" class="btn btn-default btn-lg btn-bypass-download"><span class="glyphicon glyphicon-download" aria-hidden="true"></span></button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } if(in_array('queuemanagement',$actions)){ ?>
                        <div id="queuemanagement" class="tab-pane fade">
                            <br/>
                            <?php
                                $query = "SELECT si.REF, si.system_created, si.messaggio, si.CELLULARE, sie.operation " .
                                         "FROM $database.$smsimportTable si LEFT JOIN $database.$smsimporteventTable sie ON si.REF = sie.sms_import_ref " .
                                         "WHERE si.queued = 1 AND sie.REF = (SELECT MAX(REF) FROM $database.$smsimporteventTable sie2 WHERE si.REF = sie2.sms_import_ref) " .
                                         "ORDER BY si.REF DESC";
                                $queueList = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                            ?>
                            <h2>Moderazione Coda SMS</h2>
                            <table id="queueTable" class="ui celled table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Cod.Cliente</th>
                                        <th>Data</th>
                                        <th>Indirizzo</th>
                                        <th>Circ.</th>
                                        <th>Messaggio</th>
                                        <th>Motivo</th>
                                        <th>Tel.</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Cod.Cliente</th>
                                        <th>Data</th>
                                        <th>Indirizzo</th>
                                        <th>Circ.</th>
                                        <th>Messaggio</th>
                                        <th>Motivo</th>
                                        <th>Tel.</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                foreach($queueList as $queue):
                                    $REF = $queue['REF'];
                                    $CELLULARE = str_replace('+39','',$queue['CELLULARE']);
                                    $systemCreated = date('d/m/Y H:i', strtotime($queue['system_created']));
                                    $geolocQuery = "SELECT cod_cliente, indirizzo_completo, circoscrizione FROM $database.$smsgeolocTable WHERE CELLULARE LIKE '%$CELLULARE%'";
                                    $geolocList = query(array(DBH => $udbh, sql => $geolocQuery, direct => 1, debug => $debug, status => 1));
                                    $operation = $queue['operation'];
                                    preg_match('#\((.*?)\)#', $operation, $match);
                                ?>
                                        <tr class="completed" data-ref="<?php echo $REF ?>">
                                            <td></td>
                                            <td><?php echo $geolocList[0]['cod_cliente'] ?></td>
                                            <td><?php echo $systemCreated ?></td>
                                            <td><?php echo $geolocList[0]['indirizzo_completo'] ?></td>
                                            <td><?php echo $geolocList[0]['circoscrizione'] ?></td>
                                            <td><?php echo $queue['messaggio'] ?></td>
                                            <td><?php echo $match[1] ?></td>
                                            <td><?php echo $CELLULARE ?></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="actions">
                                                    <button title="Rimetti in coda" type="button" class="btn btn-warning btn btn-lg btn-queue-resend"><span class="glyphicon glyphicon-share" aria-hidden="true"></span></button>
                                                    <button title="Segna come risolto" type="button" class="btn btn-success btn-lg btn-queue-check"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
                                                    <button title="Genera report" type="button" class="btn btn-default btn-lg btn-queue-report"><span class="glyphicon glyphicon-download" aria-hidden="true"></span></button>
                                                </div>
                                            </td>
                                            <td><?php echo $REF ?></td>
                                        </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } if(in_array('geolocmanagement',$actions)){ ?>
                        <div id="geolocmanagement" class="tab-pane fade">
                            <br/>
                            <?php
                                $query = "SELECT REF, CELLULARE, cod_cliente, indirizzo_completo, comune, circoscrizione FROM $database.$smsgeolocTable";
                                $geolocList = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                            ?>
                            <h2>Geolocalizzazione cliente</h2>
                            <?php $errorString = '';
                                  if(isset($_POST['messageRow']) AND $messageRow = $_POST['messageRow'])
                                      $errorString .= "<p>$messageRow</p>";
                                  if(isset($_POST['messageAddress']) AND $messageAddress = $_POST['messageAddress'])
                                      $errorString .= "<p>$messageAddress</p>";
                                  if($errorString !== '')
                                      echo '<div class="server_messages">'.$errorString.'</div>';
                            ?>
                            <br/>
                            <div class="geoloc_importer">
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Seleziona file</span>
                                    <input id="fileupload" type="file" name="files[]" multiple>
                                </span>
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                                <div id="files" class="files"></div>
                            </div>
                            <table id="geolocTable" class="ui celled table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Cod.Cliente</th>
                                        <th>Cellulare</th>
                                        <th>Comune</th>
                                        <th>Indirizzo Completo</th>
                                        <th>Circoscrizione</th>
                                        <th>Link</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Cod.Cliente</th>
                                        <th>Cellulare</th>
                                        <th>Comune</th>
                                        <th>Indirizzo Completo</th>
                                        <th>Circoscrizione</th>
                                        <th>Link</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                foreach($geolocList as $geoloc):
                                    $REF = $geoloc['REF'];
                                ?>
                                        <tr class="completed" data-ref="<?php echo $REF ?>">
                                            <td><?php echo $geoloc['cod_cliente'] ?></td>
                                            <td><?php echo $geoloc['CELLULARE'] ?></td>
                                            <td><?php echo $geoloc['comune'] ?></td>
                                            <td><?php echo $geoloc['indirizzo_completo'] ?></td>
                                            <td><?php echo $geoloc['circoscrizione'] ?></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="actions">
                                                    <button title="Elimina connessione" type="button" class="btn btn-danger btn-lg btn-geoloc-remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                                </div>
                                            </td>
                                        </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } if(in_array('countsmsmanagement',$actions)){ ?>
                        <div id="countsmsmanagement" class="tab-pane fade">
                            <br/>
                            <?php
                                $query = "SELECT SUM(spediti) AS spediti, COUNT(*) AS countsms, sr.idrichiedente, sr.sms_sender_title, sr.descrizione FROM $database.$smsTable s "
                                       . "LEFT JOIN $database.$smsrichiedentiTable sr USING (idrichiedente) "
                                       . "WHERE s.system_status_code = 100 AND s.system_status = 2 GROUP BY sr.idrichiedente ORDER BY countsms DESC";
                                $countList = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
                            ?>
                            <h2>SMS inviati</h2>
                            <br>
                            <table id="countsmsTable" class="ui celled table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Id Mittente</th>
                                        <th>Mittente</th>
                                        <th>Descrizione</th>
                                        <th>Transazioni</th>
                                        <th>SMS Inviati</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Id Mittente</th>
                                        <th>Mittente</th>
                                        <th>Descrizione</th>
                                        <th>Transazioni</th>
                                        <th>SMS Inviati</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                foreach($countList as $count):
                                ?>
                                        <tr class="completed">
                                            <td><?php echo $count['idrichiedente'] ?></td>
                                            <td><?php echo $count['sms_sender_title'] ?></td>
                                            <td><?php echo $count['descrizione']; if($count['idrichiedente'] == 1) echo ' <b>*</b>' ?></td>
                                            <td><?php echo $count['countsms'] ?></td>
                                            <td class='sum'><?php echo ($count['idrichiedente'] == 1) ? $count['spediti'] + 408 : $count['spediti']; ?></td>
                                        </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                            <b>*</b> aggiunti manualmente <u><i>408 SMS</i></u> inviati nel mese di giugno 2018 per test di pubblicazione modifiche, ma non presenti su DB<br/>
                            <b>N.B.:</b> la discrepanza tra il totale e la reportistica via mail dipende dall'assenza di controllo sull'effettivo invio dell'SMS da parte di quest'ultima
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../assets/jquery/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../assets/od/js/od.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/dataTables/datatables.min.js"></script>
        <script src="../assets/jQueryFileUpload/js/vendor/jquery.ui.widget.js"></script>
        <script src="../assets/jQueryFileUpload/js/jquery.iframe-transport.js"></script>
        <script src="../assets/jQueryFileUpload/js/jquery.fileupload.js"></script>
        <script src="../assets/moment/js/moment.js"></script>
        <script src="../assets/moment/js/it.js"></script>
        <script src="../assets/od/js/datetime-moment.js"></script>
        <script src="../assets/datetimepicker/js/bootstrap-datetimepicker.js"></script>
    </body>
</html>