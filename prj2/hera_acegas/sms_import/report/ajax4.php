<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

$database = 'hera_aaa_sms_rifiuti';
$table = 'sms_bypass_sap';
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
$debug = 0;

unset($_REQUEST);

foreach($_POST as $k => $v)
    $_REQUEST[$k] = iconv('UTF-8', 'ISO-8859-1', $v);
$query = "SELECT * FROM $database.$table WHERE REF = ". $_REQUEST['ref'];
$result = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
$result[0]['message'] = iconv('CP1252', 'UTF-8', $result[0]['message']);
echo json_encode($result);
?>