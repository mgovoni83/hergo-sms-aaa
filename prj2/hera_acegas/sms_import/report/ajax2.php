<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

$database = 'hera_aaa_sms_rifiuti';
$table = 'sms_bypass_sap';

unset($_REQUEST);

$campi_da_convertire = array('message'      => 'message',
                             'start'        => 'start',
                             'stop'         => 'stop',
                             'exclusionList'=> 'exclusion_list');
foreach($campi_da_convertire as $k => $v)
    $_REQUEST[$v] = addslashes(iconv('UTF-8', 'ISO-8859-1', $_POST[$k]));
$params = array(database => $database, table => $table, debug => 0);
if($_POST['ref']){
    $params['ref'] = $_POST['ref'];
    $params['type'] = "edit";
}
else
    $params['type'] = "add";
$_SESSION['autorizzazione_salvataggio'] = time()-60;
$retval = control_form_handler($params);	# scrivo i dati su DB
if(!$retval)
    $output = array('status' => '0', 'message' => 'Impossibile salvare i dati! Si prega di riprovare pi&ugrave; tardi.');
else
    $output = array('status' => '1', 'message' => 'Salvataggio completato!');
echo json_encode($output);
?>