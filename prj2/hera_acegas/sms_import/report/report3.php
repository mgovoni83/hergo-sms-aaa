<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
if(isset($_GET['refs']) AND $refs = $_GET['refs']){
    $codici_lingua = array("it" => 1, "en" => 2);
    require_once("pwd/SUPER_USER_CONNECT.php");
    require_once("pwd/Hera.php");
    require_once("super_connect.php");
    require_once("sms/lib-mobytsms.inc.php");
    if(!class_exists('soapclient'))
        require_once("sms/lib-nusoap.inc.php");
    require_once("adm/sms_tracker.php");
    require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
    require_once("/server/prj2/hera_acegas/sms_import/_functions.php");
    $udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
    $myDatabase = 'hera_aaa_sms_rifiuti';
    $smsgeolocTable = 'sms_geoloc';
    $smsimportTable = 'sms_import';
    $smsimporteventTable = 'sms_import_event';

    $udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
    $siQuery = "SELECT si.CELLULARE, si.system_created, si.messaggio, sie.operation FROM $myDatabase.$smsimportTable si " .
               "LEFT JOIN $myDatabase.$smsimporteventTable sie ON si.REF = sie.sms_import_ref " .
               "WHERE si.REF in ($refs) AND sie.REF = (SELECT MAX(REF) FROM $myDatabase.$smsimporteventTable sie2 WHERE si.REF = sie2.sms_import_ref) " .
               "ORDER BY si.REF DESC";
    $siList = query(array(DBH => $udbh, sql => $siQuery, direct => 1, debug => $debug, status => 1));

    $data = array();
    $data[] = array('Num. Telefono',
                    'Cod. Cliente',
                    'Data',
                    'Indirizzo',
                    'Comune',
                    'Circoscrizione',
                    'Motivo',
                    'Messaggio');
    foreach($siList as $si){
        $operation = $si['operation'];
        preg_match('#\((.*?)\)#', $operation, $match);
        $CELLULARE = str_replace('+39','',$si['CELLULARE']);
        $systemCreated = date('d/m/Y H:i', strtotime($si['system_created']));
        $sgQuery = "SELECT cod_cliente, indirizzo_completo, comune, circoscrizione FROM $myDatabase.$smsgeolocTable WHERE CELLULARE LIKE '%$CELLULARE%'";
        $sgList = query(array(DBH => $udbh, sql => $sgQuery, direct => 1, debug => $debug, status => 1));
        $data[] = array($CELLULARE,
                        $sgList[0]['cod_cliente'],
                        $systemCreated,
                        $sgList[0]['indirizzo_completo'],
                        $sgList[0]['comune'],
                        $sgList[0]['circoscrizione'],
                        $match[1],
                        $si['messaggio']);
    }

    $filename = "queue-report";
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename={$filename}.csv");
    header("Pragma: no-cache");
    header("Expires: 0");
    outputCSV($data);
}
else
    echo 'Nessuna reportistica disponibile! Specificare i messaggi in coda desiderati.'
?>