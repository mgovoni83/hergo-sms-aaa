<?php
function remove_utf8_bom($text)
{
    $bom = pack('H*','EFBBBF');
    $text = preg_replace("/^$bom/", '', $text);
    return $text;
}
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

$database = 'hera_aaa_sms_rifiuti';
$table = 'sms_geoloc';
$streetmapTable = 'sms_street_map';
$debug = 0;
$now = date("Y-m-d H:i:s");
$error = false;
$messageRowArray = $messageAddressArray = array();
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));

$filename = "files/".$_POST['filename'];
$output = str_replace(array('|', "\t"), ';', file_get_contents($filename));
file_put_contents($filename, $output);
if(($handle = fopen($filename, "r")) !== FALSE){
    $row = 0;
    while(($data = fgetcsv($handle, 1000, ";")) !== FALSE){
        $row++;
        if($row == 1){
            continue;
        }
        if (count($data) < 6){
            $messageRowArray[] =  $row;
            continue;
        }
        $CELLULARE          = preg_replace('/\D/', '', preg_replace('/[\x00-\x1F\x7F]/', '',$data[1]));
        $codCliente         = preg_replace('/\D/', '', preg_replace('/[\x00-\x1F\x7F]/', '',$data[2]));
        $indirizzoCompleto  = addslashes(str_replace('"','',preg_replace('/[\x00-\x1F\x7F]/', '',$data[5])));
        $comune             = addslashes(str_replace('"','',preg_replace('/[\x00-\x1F\x7F]/', '',$data[4])));
        
        $streetPieces = explode(',',$indirizzoCompleto);
        $street = $streetPieces[0];
        $string = array_filter(preg_split("/\D+/", $streetPieces[1]));
        $civico = reset($string);
        $streetmapQuery = "SELECT circoscrizione, civico_start, civico_stop, civico_pari FROM $database.$streetmapTable WHERE comune = '$comune' AND LOWER(street) LIKE '". strtolower($street) ."'";
        $streetmapResult = query(array(DBH => $udbh, sql => $streetmapQuery, direct => 1, debug => $debug, status => 1));
        $countResult = count($streetmapResult);
        if($comune == 'PADOVA' || $comune == 'TRIESTE' ){
            # Nessun risultato = errore + aggiornamento messaggio
            if($countResult < 1){
                $messageAddressArray[] = $codCliente;
                continue;
            }
            /* Su Trieste è possibile ottenere più risultati, in quanto alcune vie
             * appartengono a più cisrcoscrizioni. Per ottenere quella corretta
             * analizzo il civico dell'indirizzo
             */
            elseif($countResult > 1){
                foreach($streetmapResult as $streetmapRow){
                    if($civico >= $streetmapRow['civico_start'] AND $civico <= $streetmapRow['civico_stop']){
                        if($civico % 2 == 0 AND $streetmapRow['civico_pari'] == 2)
                            $circoscrizione = iconv('CP1252', 'UTF-8', $streetmapRow['circoscrizione']);
                        if($civico % 2 == 1 AND $streetmapRow['civico_pari'] == 1)
                            $circoscrizione = iconv('CP1252', 'UTF-8', $streetmapRow['circoscrizione']);
                    }
                }
            }
            # Ottengo un solo risultato, perciò non occorre alcuna post-elaborazione
            else
                $circoscrizione = (isset($streetmapResult[0]['circoscrizione'])) ? iconv('CP1252', 'UTF-8', $streetmapResult[0]['circoscrizione']) : '';
        }
        else
            $circoscrizione = '';
        $query = "INSERT INTO $database.$table (system_created, system_modified, CELLULARE, cod_cliente, indirizzo_completo, comune, circoscrizione) "
               . "VALUES ('$now', '$now', $CELLULARE, $codCliente, '$indirizzoCompleto', '$comune', '$circoscrizione') "
               . "ON DUPLICATE KEY UPDATE system_modified = '$now', CELLULARE = $CELLULARE, cod_cliente = $codCliente, indirizzo_completo = '$indirizzoCompleto', comune = '$comune', circoscrizione = '$circoscrizione'";
        if($result = dbdo(array(QUERY_1 => $query, DBH => $GLOBALS['DBH'])))
            $counter++;
        else{
            $error = true;
            break;
        }
    }
    fclose($handle);
}

$result = array();
$result['status'] = $error ? '0' : '1';
$result['message']['error'] = $error ? 'Impossibile completare l\'operazione: importazione sospesa alla riga '. ($row + 1) : '';
$result['message']['errorRow'] = count($messageRowArray) ? 'La seguente riga non è conforme ed è stata scartata: '. implode(', ', $messageRowArray) : '';
$result['message']['errorAddress'] = count($messageAddressArray) ? 'Gli indirizzi dei seguenti codici cliente non hanno trovato riscontro sullo stradario: '. implode(', ', $messageAddressArray) : '';
echo json_encode($result);
?>