<?php
session_start();
if(!isset($_SESSION['login'])){
    $login = false;
?>
<?php
    $codici_lingua = array("it" => 1, "en" => 2);
    require_once("pwd/SUPER_USER_CONNECT.php");
    require_once("pwd/Hera.php");
    require_once("super_connect.php");
    require_once("sms/lib-mobytsms.inc.php");
    if(!class_exists('soapclient'))
        require_once("sms/lib-nusoap.inc.php");
    require_once("adm/sms_tracker.php");
    require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
    $GLOBALS[SESSION_DEBUG]=0;
    start_netbox_session();
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <script type="text/javascript">var login = false</script>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <title>Reportistica SAP Bypass</title>
            <!-- Bootstrap -->
            <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <link href="../assets/od/css/od.css" rel="stylesheet">
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        </head>
        <body>
    <!-- BEGIN # MODAL LOGIN -->
            <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="login-container">
                        <div id="output"></div>
                        <div class="form-box">
                            <h1>Login utente</h1>
                            <form method="GET">
                                <input name="user" type="text" placeholder="username" />
                                <input name="password" type="password" placeholder="password" />
                                <input name="hash" type="text" placeholder="hash" />
                                <button class="btn btn-info btn-block login" type="submit">Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    <!-- END # MODAL LOGIN -->
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="../assets/jquery/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
            <!--<script src="../assets/od/js/od.js"></script>-->
            <script>
                jQuery(function($){
                    $("#login-modal").modal({backdrop:'static', keyboard:false});
                    //  Procedura login
                    var userfield = $("#login-modal input[name=user]");
                    var passwordfield = $("#login-modal input[name=password]");
                    $('#login-modal button[type="submit"]').click(function(e) {
                        e.preventDefault();
                        if (userfield.val() == ""){
                            $("#login-modal #output").removeClass(' alert alert-success');
                            $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html("Devi inserire il nome utente");
                        }
                        else if(passwordfield.val() == "") {
                            $("#login-modal #output").removeClass(' alert alert-success');
                            $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html("Devi inserire la password");
                        }
                        else {
                            $.post("ajax.php", $("#login-modal form").serialize())
                            .done(function(data){
                                var result = JSON.parse(data);
                                if(parseInt(result.status)){
                                    $("#login-modal #output").removeClass('alert-danger');
                                    $("#login-modal #output").addClass("alert alert-success animated fadeInUp").html(result.message);
                                    $("#login-modal").modal('hide');
                                    $(result.html).insertAfter(".nav-tabs .dashboard");
                                    $(".main-container").removeClass('hidden');
                                    login = parseInt(result.login);
                                    location.replace('report.php');
                                }
                                else{
                                    $("#login-modal #output").removeClass('alert alert-success');
                                    $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html(result.message);
                                }
                            })
                            .fail(function(){
                                $("#login-modal #output").removeClass('alert alert-success');
                                $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html('Problema di connessione al database, Riprova pi&ugrave; tardi!');
                            });
                        }
                    });
                });
                //  Procedura login
                var userfield = $("#login-modal input[name=user]");
                var passwordfield = $("#login-modal input[name=password]");
                $('#login-modal button[type="submit"]').click(function(e) {
                    e.preventDefault();
                    if (userfield.val() == ""){
                        $("#login-modal #output").removeClass(' alert alert-success');
                        $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html("Devi inserire il nome utente");
                    }
                    else if(passwordfield.val() == "") {
                        $("#login-modal #output").removeClass(' alert alert-success');
                        $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html("Devi inserire la password");
                    }
                    else {
                        $.post("ajax.php", $("#login-modal form").serialize())
                        .done(function(data){
                            var result = JSON.parse(data);
                            if(parseInt(result.status)){
                                $("#login-modal #output").removeClass('alert-danger');
                                $("#login-modal #output").addClass("alert alert-success animated fadeInUp").html(result.message);
                                $("#login-modal").modal('hide');
                                $(result.html).insertAfter(".nav-tabs .dashboard");
                                $(".main-container").removeClass('hidden');
                                login = parseInt(result.login);
                                location.replace('report.php');
                            }
                            else{
                                $("#login-modal #output").removeClass('alert alert-success');
                                $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html(result.message);
                            }
                        })
                        .fail(function(){
                            $("#login-modal #output").removeClass('alert alert-success');
                            $("#login-modal #output").addClass("alert alert-danger animated fadeInUp").html('Problema di connessione al database, Riprova pi&ugrave; tardi!');
                        });
                    }
                });
            </script>
        </body>
    </html>
<?php } else header('Location: report.php', true); ?>