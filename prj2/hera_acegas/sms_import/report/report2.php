<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
if(isset($_GET['ref'])){
    $codici_lingua = array("it" => 1, "en" => 2);
    require_once("pwd/SUPER_USER_CONNECT.php");
    require_once("pwd/Hera.php");
    require_once("super_connect.php");
    require_once("sms/lib-mobytsms.inc.php");
    if(!class_exists('soapclient'))
        require_once("sms/lib-nusoap.inc.php");
    require_once("adm/sms_tracker.php");
    require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
    require_once("/server/prj2/hera_acegas/sms_import/_functions.php");

    $GLOBALS[SESSION_DEBUG]=0;
    start_netbox_session();
    $udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
    $debug = 0;
    $myDatabase = 'hera_aaa_sms_rifiuti';
    $myTable = 'sms';
    unset($_REQUEST);

    $udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
    $ref = $_GET['ref'];
    $query = "SELECT system_created, CELLULARE FROM $myDatabase.$myTable WHERE idbypass = $ref ORDER BY REF ASC";
    $cellList = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
    if(count($cellList)){
        $data = array();
        foreach($cellList as $cell)
            $data[] = array($cell['system_created'],$cell['CELLULARE']);
        $filename = "report";
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        outputCSV($data);
    }
    else
        echo 'Nessuna reportistica disponibile per il blocco SAP specificato!';
}
else
    echo 'Nessuna reportistica disponibile! Specificare il blocco SAP desiderato.';
?>