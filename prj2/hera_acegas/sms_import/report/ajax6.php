<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

$database = 'hera_aaa_sms_rifiuti';
$smsSettingsTable = 'sms_settings';

$emails = implode(',', array_filter($_POST['emails']));
$KOSMS = addslashes(htmlentities(utf8_decode($_POST['KOSMS'])));
$KOPI = addslashes(htmlentities(utf8_decode($_POST['KOPI'])));
$KOAG = addslashes(htmlentities(utf8_decode($_POST['KOAG'])));
if (dbdo(array(QUERY_1 => "UPDATE $database.$smsSettingsTable SET value = '$emails' WHERE config = 'email_notification'", DBH => $GLOBALS['DBH'])) AND
    dbdo(array(QUERY_1 => "UPDATE $database.$smsSettingsTable SET value = '$KOSMS' WHERE config = 'ko_sms'", DBH => $GLOBALS['DBH'])) AND
    dbdo(array(QUERY_1 => "UPDATE $database.$smsSettingsTable SET value = '$KOPI' WHERE config = 'ko_pi'", DBH => $GLOBALS['DBH'])) AND
    dbdo(array(QUERY_1 => "UPDATE $database.$smsSettingsTable SET value = '$KOAG' WHERE config = 'ko_agenda'", DBH => $GLOBALS['DBH'])))
    $output = array('status' => '1', 'message' => 'Modifica configurazione completata!');
else{
    mail('matteo.govoni@dedagroup.it','Alert from HERGO SMS','Problema di salvataggio configurazione (db: hera_aaa_sms_rigiuti; table: sms_settings)');
    $output = array('status' => '0', 'message' => 'Impossibile salvare la configurazione! Si prega di riprovare pi&ugrave; tardi.');
}
echo json_encode($output);
?>