<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

$database = 'hera_aaa_sms_rifiuti';
$table = 'sms_geoloc';

unset($_REQUEST);

foreach($_POST as $k => $v)
    $_REQUEST[$k] = iconv('UTF-8', 'ISO-8859-1', $v);
$_SESSION['autorizzazione_salvataggio'] = time()-60;
$params = array(database => $database, table => $table, debug => 0);
$params['ref'] = $_POST['ref'];
$params['type'] = "del";
$retval = control_delete_record($params);
if(!$retval)
    $output = array('status' => '0', 'message' => 'Impossibile eliminare i dati! Si prega di riprovare pi&ugrave; tardi.');
else
    $output = array('status' => '1', 'message' => 'Cancellazione completata!');
echo json_encode($output);
?>