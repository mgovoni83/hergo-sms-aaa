<?php
session_start();
if(!isset($_SESSION['login']))
    header('Location: login.php', true);
?>
<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
require_once("Excel/reader.php");

$database = 'hera_aaa_sms_rifiuti';
$table = 'sms_geoloc';
$debug = 0;

$data = new Spreadsheet_Excel_Reader();
$data->setOutputEncoding('CP1251');

$filename = $_POST['filename'];
$data->read('files/'.$filename);

$now = date("Y-m-d H:i:s");
$counter = 0;
$error = false;
for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++){
    $CELLULARE          = addslashes($data->sheets[0]['cells'][$i][2]);
    $codCliente         = addslashes($data->sheets[0]['cells'][$i][3]);
    $comune             = addslashes($data->sheets[0]['cells'][$i][5]);
    $indirizzoCompleto  = addslashes($data->sheets[0]['cells'][$i][6]);
    $indirizzo          = addslashes($data->sheets[0]['cells'][$i][7]);
    $circoscrizione     = addslashes($data->sheets[0]['cells'][$i][8]);
    $query = "INSERT INTO $database.$table (system_created, system_modified, CELLULARE, cod_cliente, indirizzo, indirizzo_completo, comune, circoscrizione) "
           . "VALUES ('$now', '$now', '$CELLULARE', '$codCliente', '$indirizzo', '$indirizzoCompleto', '$comune', '$circoscrizione') "
           . "ON DUPLICATE KEY UPDATE system_modified = '$now', cod_cliente = '$codCliente', indirizzo = '$indirizzo', indirizzo_completo = '$indirizzoCompleto', comune = '$comune', circoscrizione = '$circoscrizione'";
    if($result = dbdo(array(QUERY_1 => $query, DBH => $GLOBALS['DBH'])))
        $counter++;
    else{
        $error = true;
        break;
    }
}

$result = array();
$result['status'] = $error ? '0' : '1';
$result['message'] = $error ? 'Impossibile completare l\'importazione, contattare un amministratore di sistema!' : '';
echo json_encode($result);
?>