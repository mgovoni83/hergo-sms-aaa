<?php
ini_set("auto_detect_line_endings", true);
include_once('/server/prj2/hera_intranet/rifiutologo_wsdl/Classes/PHPExcel/IOFactory.php');
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
	require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

$GLOBALS[SESSION_DEBUG]=0;
start_netbox_session();
$debug = 0;	
global $database;
$database = "hera_aaa_sms_rifiuti";
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));

$retCode = $details = 'CRON REPORT';

$start_month_time = strtotime("first day of previous month midnight");
$end_month_time = strtotime("first day of this month midnight") - 1;
$start_month = date("Y.m.d",$start_month_time);
$end_month = date("Y.m.d",$end_month_time);

$query = "SELECT idrichiedente, email, descrizione, bcc FROM $database.sms_richiedenti WHERE sms_richiedenti.report_frequency LIKE '%m%' AND email <> ''";
$richiedenti = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
foreach($richiedenti as $richiedente){
    $queryReport = "SELECT sms.system_created, sms.system_status, CELLULARE, messaggio, mobyt_code_translated_label AS code_label FROM $database.sms "
                 . "LEFT JOIN $database.sms_mobyt_code ON sms_mobyt_code.mobyt_code = sms.system_status_code "
                 . "WHERE (sms.system_created BETWEEN FROM_UNIXTIME($start_month_time) AND FROM_UNIXTIME($end_month_time)) AND idrichiedente = ". $richiedente['idrichiedente'];
    $rows = query(array(DBH => $udbh, sql => $queryReport, direct => 1, debug => $debug, status => 1));
    foreach($rows as $row){
        switch($row['system_status']){
            case 0:
                $system_status = 'non inviato';
            break;
            case 1:
                $system_status = 'in coda';
            break;
            case 2:
                $system_status = 'inviato';
            break;
        }
        $csv[$richiedente['idrichiedente']]['data'][] = array($row['system_created'],$system_status,$row['CELLULARE'],str_replace(array("\n","\t","\r"),' ',$row['messaggio']),$row['code_label']);
        $csv[$richiedente['idrichiedente']]['email'] = $richiedente['email'];
        $csv[$richiedente['idrichiedente']]['timeslot'] = 'Report mensile ('. $start_month.' - '.$end_month .')';
        $csv[$richiedente['idrichiedente']]['type'] = 'm';
        $csv[$richiedente['idrichiedente']]['filename'] = date("Ymd",$start_month_time).'-'.date("Ymd",$end_month_time);
        $csv[$richiedente['idrichiedente']]['descrizione'] = $richiedente['descrizione'];
        $csv[$richiedente['idrichiedente']]['bcc'] = $richiedente['bcc'];
    }
}

foreach($csv as $idrichiedente => $reportFile){
    $dir = 'csv/'.$reportFile['type'].'/'.$idrichiedente.'/';
    if (!file_exists($dir))
        mkdir($dir, 0755, true);
    $fp = fopen($dir.$reportFile['filename'].'.csv', 'w+');
    fputcsv($fp, array('data creazione','stato consegna','numero cellulare','messaggio','stato'));
    foreach ($reportFile['data'] as $fields)
        fputcsv($fp, $fields);
    fclose($fp);

    $objReader = PHPExcel_IOFactory::createReader('CSV');
    $objReader->setDelimiter(",");
    $objReader->setInputEncoding('ISO-8859-1');
    $objReader->setReadDataOnly(false);
    $objPHPExcel = $objReader->load($dir.$reportFile['filename'].'.csv');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $dir = 'xls/'.$reportFile['type'].'/'.$idrichiedente.'/';
    if (!file_exists($dir))
        mkdir($dir, 0755, true);
    $objWriter->save($dir.$reportFile['filename'] .'.xls');

    $filename = str_replace('/nfs/gruppohera','/server', dirname(__FILE__)) .'/'. $dir . $reportFile['filename'];
    $attachments = array($reportFile['filename'] => $filename.'.xls');

    $subject = $reportFile['timeslot'];
    $msg = 'Report da Officine Digitali per l\'invio SMS del servizio '.$reportFile['descrizione'].'.';
    mail64($reportFile['email'], $subject, $msg, 'Reportistica Invio SMS <noreply@gruppohera.it>', 1, 1, $attachments, $reportFile['bcc']);
}