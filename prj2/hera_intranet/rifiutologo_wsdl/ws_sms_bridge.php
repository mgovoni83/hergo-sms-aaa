<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
require_once("/server/prj2/hera_acegas/sms_import/_functions.php");

$GLOBALS[SESSION_DEBUG]=0;
start_netbox_session();
$debug = 0;	
global $database;
$database = "hera_aaa_sms_rifiuti";
$smsImportTable = "sms_import";
$smsBypassSapTable = "sms_bypass_sap";
$smsSettingsTable = "sms_settings";
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));

$retCode = $details = 'CRON BRIDGE';

# Accodamento messaggi bloccati da più di 28 minuti causa Siebel PI
$query = "SELECT REF,CELLULARE FROM $database.$smsImportTable" .
        " WHERE system_status = 0" .
        " AND checked = 0" .
        " AND queued = 0" .
        " AND system_modified < timestamp(DATE_SUB(NOW(), INTERVAL 28 MINUTE))";
$queues = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
foreach($queues as $queue){
    $smsImportRef = $queue['REF'];
    if(dbdo(array(QUERY_1 => "UPDATE $database.$smsImportTable SET queued = 1, system_modified = '". date("Y-m-d H:i:s") ."' WHERE REF = $smsImportRef", DBH => $GLOBALS['DBH']))){
        recordAction($smsImportRef, 'QUEUED (KO PI)');
        $queryMsg = "SELECT value FROM $database.$smsSettingsTable WHERE config = 'ko_pi'";
        $resultMsg = query(array(DBH => $udbh, sql => $queryMsg, direct => 1, debug => $debug, status => 1));
        $msg = $resultMsg[0]['value'];
        sendCourtesyMsg($queue['CELLULARE'], $msg);
    }
}

# Accodamento messaggi bloccati da più di 28 minuti causa Siebel Agenda
$query = "SELECT REF, CELLULARE FROM $database.$smsImportTable" .
        " WHERE system_status = 1" .
        " AND checked = 0" .
        " AND queued = 0" .
        " AND system_modified < timestamp(DATE_SUB(NOW(), INTERVAL 28 MINUTE))";
$queues = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
foreach($queues as $queue){
    $smsImportRef = $queue['REF'];
    if(dbdo(array(QUERY_1 => "UPDATE $database.$smsImportTable SET system_status = 0, queued = 1, system_modified = '". date("Y-m-d H:i:s") ."' WHERE REF = $smsImportRef", DBH => $GLOBALS['DBH']))){
        recordAction($smsImportRef, 'QUEUED (KO Agenda)');
        $queryMsg = "SELECT value FROM $database.$smsSettingsTable WHERE config = 'ko_agenda'";
        $resultMsg = query(array(DBH => $udbh, sql => $queryMsg, direct => 1, debug => $debug, status => 1));
        $msg = $resultMsg[0]['value'];
        sendCourtesyMsg($queue['CELLULARE'], $msg);
    }
}

# Invio a Siebel dei messaggi ricevuti
$query = "SELECT REF, messaggio, CELLULARE, token FROM $database.$smsImportTable" .
        " WHERE system_status = 0" .
        " AND checked = 0" .
        " AND queued = 0";
$messages = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
foreach($messages as $message){
    $cellulare = str_replace('+39','',$message['CELLULARE']);
    $messaggio = $message['messaggio'];
    $smsImportRef = $message['REF'];
//    mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','OK 1');
    if(myCheckSMS($messaggio)){
//        mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','OK 2');
        $token = $message['token'];
        $now = strtotime('now');
        $queryBypassTel = "SELECT REF,message FROM $database.$smsBypassSapTable WHERE exclusion_list LIKE '%$cellulare%' AND ". $now ." BETWEEN coalesce(start,". $now .") AND coalesce(stop,". $now .")";
        $sms_bypass_tel = query(array(DBH => $udbh, sql => $queryBypassTel, direct => 1, debug => $debug, status => 1));
        $queryBypass = "SELECT REF,message FROM $database.$smsBypassSapTable WHERE exclusion_list = '' AND ". $now ." BETWEEN coalesce(start,". $now .") AND coalesce(stop,". $now .")";
        $sms_bypass = query(array(DBH => $udbh, sql => $queryBypass, direct => 1, debug => $debug, status => 1));
        if(count($sms_bypass_tel)){
            if(bypassSAP($cellulare,$smsImportRef,$sms_bypass_tel[0]['REF'],$sms_bypass_tel[0]['message']) === false)
                mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','Riscontrato problema su servizio bypass: '. $cellulare .' - '. $smsImportRef .' (sms_import) - '. $sms_bypass_tel[0]['REF'] .'(sms_bypass_sap)');
        }
        elseif(count($sms_bypass)){
            if(bypassSAP($cellulare,$smsImportRef,$sms_bypass[0]['REF'],$sms_bypass[0]['message']) === false)
                mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','Riscontrato problema su servizio bypass: '. $cellulare .' - '. $smsImportRef .' (sms_import) - '. $sms_bypass[0]['REF'] .' (sms_bypass_sap)');
        }
        else{
            $username = 'BS_008_001';
            $password = 'Famulaon1';
    //      Endpoint di Produzione
	/*
            if($cellulare == '3475266350'){
                $sap_endpoint = 'http://' . $username . ':' . $password . '@piwdixiq.service.intra:8133/XISOAPAdapter/MessageServlet?senderParty=&senderService=BS_PI_WEBSERVICE_QLTY&receiverParty=&receiverService=&interface=os_SOAP_SSA_HERGOReceiveSMS&interfaceNamespace=http://Hera.it/SSA_HERGO_SMS';
                mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','OK 3');
            }
            else*/
                $sap_endpoint = 'http://' . $username . ':' . $password . '@piwdixip.service.intra:8133/XISOAPAdapter/MessageServlet?senderParty=&senderService=BS_PI_WEBSERVICE_PROD&receiverParty=&receiverService=&interface=os_SOAP_SSA_HERGOReceiveSMS&interfaceNamespace=http://Hera.it/SSA_HERGO_SMS';

            $post_data = '<?xml version="1.0" encoding="utf-8"?>
                            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ssa="http://Hera.it/SSA_HERGO_SMS">
                                <soapenv:Header/>
                                <soapenv:Body>
                                   <ssa:MT_SSA_HERGOReceiveSMSReq>
                                      <TestoSMS>'.$messaggio.'</TestoSMS>
                                      <NumeroTelefono>'.$cellulare.'</NumeroTelefono>
                                      <Token>'. $token .'</Token>
                                   </ssa:MT_SSA_HERGOReceiveSMSReq>
                                </soapenv:Body>
                            </soapenv:Envelope>';
//                mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','OK 4: '.$sap_endpoint.' - '.$post_data);
            if($content = get_url($sap_endpoint, $post_data)){
//                mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS','OK 5 - '.$content);
                $xml = simplexml_load_string($content);
                $xml = $xml->children('SOAP', true)->Body->children('ns0', true)->MT_SSA_HERGOReceiveSMSResp->children();
                foreach($xml as $label => $value)
                    $result[$label] = $value;
                if($result['DataPresaInCarico'] AND dbdo(array(QUERY_1 => "UPDATE $database.$smsImportTable SET system_status = 1, checked = 0, queued = 0, system_modified = '". date("Y-m-d H:i:s") ."' WHERE REF = $smsImportRef", DBH => $GLOBALS['DBH'])))
                    recordAction($smsImportRef, 'OK PI');
                else
                    mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS (test)','Riscontrato problema su risposta PI servizio: '. $smsImportRef .' (sms_import)'. print_r($result, true));
            }
            else
                mail('matteo.govoni@dedagroup.it','Avviso HERGO SMS (test)','Riscontrato problema su dialogo con SIEBEL: '. $smsImportRef .' (sms_import)');
        }
    }
    elseif(dbdo(array(QUERY_1 => "UPDATE $database.$smsImportTable SET queued = 1, system_modified = '". date("Y-m-d H:i:s") ."' WHERE REF = $smsImportRef", DBH => $GLOBALS['DBH']))){
        recordAction($smsImportRef, 'QUEUED (KO SMS)');
        $queryMsg = "SELECT value FROM $database.$smsSettingsTable WHERE config = 'ko_sms'";
        $resultMsg = query(array(DBH => $udbh, sql => $queryMsg, direct => 1, debug => $debug, status => 1));
        $msg = $resultMsg[0]['value'];
        sendCourtesyMsg($cellulare, $msg, false);
    }
}
?>