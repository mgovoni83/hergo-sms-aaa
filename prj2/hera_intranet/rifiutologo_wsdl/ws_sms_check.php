<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
require_once("/server/prj2/hera_acegas/sms_import/_functions.php");

$GLOBALS[SESSION_DEBUG]=0;
start_netbox_session();
$debug = 0;	
global $database;
$database = "hera_aaa_sms_rifiuti";
$smsImportTable = "sms_import";
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));

$query = "SELECT COUNT(*) as sms_counter FROM $database.$smsImportTable WHERE system_created < NOW() AND system_created > DATE_SUB(NOW(), INTERVAL 30 MINUTE)";
$rows = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
$smsImportCounter = $rows[0]['sms_counter'];
if($smsImportCounter == 0)
    mail('matteo.govoni@dedagroup.it,davide.cremonini@dedagroup.it','Avviso HERGO SMS - Fallita ricezione','Riscontrato problema di ricezione SMS');
?>