<?php
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");
require_once("/server/prj2/hera_acegas/sms_import/_functions.php");

function rispostaPrenotazioneRitiro(){
    global $riusciti;
    global $falliti;

    $retCode = 'OK';

    $GLOBALS[SESSION_DEBUG]=0;
    start_netbox_session();
    $debug = 0;

    global $database;
    $database = "hera_aaa_sms_rifiuti";
    $udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));
    $query = "SELECT CELLULARE,messaggio,REF,idrichiedente FROM $database.sms WHERE sms.system_status=1 ORDER BY REF DESC";
    $utenti = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));

    if(count($utenti)){
        $x1 = "'" . addslashes("CRON ALERTING DEL ". date('Y/m/d H:i:s')) . "'";
        $smstracker = new sms_tracker($database, "'SMS Servizi'", $x1, $x1, "'SMS Servizi'");
        $riusciti = $falliti = 0;

        foreach($utenti as $utente){
            $string = str_replace(array('&agrave;','&egrave;','&eacute;','&igrave;','&ograve;','&ugrave;'),array("a'","e'","e'","i'","o'","u'"),htmlentities($utente['messaggio']));
            $querySender = "SELECT sms_sender_title FROM $database.sms_richiedenti WHERE sms_richiedenti.idrichiedente = ".$utente['idrichiedente'];
            $sms_sender_title = query(array(DBH => $udbh, sql => $querySender, direct => 1, debug => $debug, status => 1));
            $sender = $sms_sender_title[0]['sms_sender_title'];
            /* Switch Mobyt da effettuare il 13/03/19
            $sms = new mobytSmsOD($database, $sender, $udbh);
             */
            $sms = new mobytSmsOD($database, $sender, $udbh, 'AAA_SMS');
            $uid = $smstracker->prepare_row();
            $code = $smstracker->get_code($uid);
            $numero = (strpos($utente['CELLULARE'], '+39') !== 0 AND strpos($utente['CELLULARE'], '0039') !== 0) ? '+39'.$utente['CELLULARE'] : $utente['CELLULARE'];
            $result = $sms->sendSmsAAA($numero, $string, $utente['REF']);
            /* Switch Mobyt da effettuare il 13/03/19
            $result = json_decode($result);
            $result = $sms->sendSmsAAA($numero, $string, $utente['REF']);
            if($result['count'] > 0 AND dbdo(array(QUERY_1 => "UPDATE $database.sms SET system_status='2', spediti = ((CHAR_LENGTH(messaggio) DIV 154) +1), system_status_code=".$result['code']." WHERE REF = '".$utente['REF']."'", DBH => $GLOBALS['DBH']))){
            if(strtoupper($result->result) == 'OK' AND $result->total_sent > 0 AND dbdo(array(QUERY_1 => "UPDATE $database.sms SET system_status='2', spediti = ((CHAR_LENGTH(messaggio) DIV 154) +1), system_status_code=". $result->order_id ." WHERE REF = '".$utente['REF']."'", DBH => $GLOBALS['DBH']))){
            error_log(print_r($result, true), 1, "matteo.govoni@dedagroup.it");
//                dbdo(array(QUERY_1 => "UPDATE $database.sms SET system_status='0', spediti = 0, system_status_code=". $result->order_id ." WHERE REF = '". $utente['REF'] ."'", DBH => $GLOBALS['DBH']));
            */
            if($result['count'] > 0 AND dbdo(array(QUERY_1 => "UPDATE $database.sms SET system_status='2', spediti = ((CHAR_LENGTH(messaggio) DIV 154) +1), system_status_code=".$result['code']." WHERE REF = '".$utente['REF']."'", DBH => $GLOBALS['DBH']))){
                $st = $result['count'];
                $riusciti++;
                $retCode = "OK";
            }
            else{
                dbdo(array(QUERY_1 => "UPDATE $database.sms SET system_status='0', spediti = 0, system_status_code=".$result['code']." WHERE REF = '".$utente['REF']."'", DBH => $GLOBALS['DBH']));
                $st = 3;
                $falliti++;
                $retCode = "KO";
            }
            $userdata = $numero . " - Comunicazione data/ora appuntamento ritiro";
            $smstracker->update_row($uid, $userdata, $st);
            $smstracker->finalize($riusciti, $falliti);
        }
    }
    return;
}

function prenotazioneRitiro($args) {
    $disallowed = array(999);
    $args = (array)$args;
    $myDatabase = 'hera_aaa_sms_rifiuti';
    $myTable = 'sms';
    $smsImportTable = 'sms_import';
    $retCode = 'OK';
    unset($_REQUEST);
    $campi_da_convertire = array('Message' => 'messaggio',
                                 'Cell' => 'CELLULARE',
                                 'Data' => 'data',
                                 'IdSap' => 'token',
                                 'IdRichiedente' => 'idrichiedente');
    foreach($campi_da_convertire as $k => $v)
        $_REQUEST[$v] = iconv('UTF-8', 'ISO-8859-1', $args[$k]);
    $token = $_REQUEST['token'];
    $data = $_REQUEST['data'];
    $_REQUEST['ipaddress'] = $_SERVER['REMOTE_ADDR'];
    $_REQUEST['system_status'] = (isset($_REQUEST['idrichiedente']) AND !in_array($_REQUEST['idrichiedente'], $disallowed)) ? 1 : 0;
    $params = array(database => $myDatabase, table => $myTable, debug => 0);
    $params['result'] = 'newref';
    $params['type'] = "add";
    $_SESSION['autorizzazione_salvataggio'] = time()-60;
    if($retval = control_form_handler($params)){    # scrivo i dati su DB
        if($token AND (empty($data) OR myCheckDate($data))){
            $query = "SELECT REF FROM $myDatabase.$smsImportTable WHERE token = '$token'";
            $rows = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
            $smsImportRef = $rows[0]['REF'];
            dbdo(array(QUERY_1 => "UPDATE $myDatabase.$smsImportTable SET checked = 1, system_modified = '". date("Y-m-d H:i:s") ."' WHERE REF = $smsImportRef", DBH => $GLOBALS['DBH']));
            recordAction($smsImportRef, 'OK');
        }
    }
    else{
        mail('matteo.govoni@dedagroup.it','HERGO SMS',"Error");
        $retCode = 'KO';
    }
    return array("operationResult" => $retCode, "timestamp" => date('d/m/Y H:i:s'), "token" => $token);
}

// --------------------------------------------------------------------------------------------------------------
// loggo le richieste
$xmlRichiesta = file_get_contents("php://input");
file_put_contents('logs/req_'.date('YmdHis').'.log', $xmlRichiesta);

// SERVER
ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
$server = new SoapServer("rifiutologo_sms.wsdl");
$server->addFunction("prenotazioneRitiro");
$server->addFunction("rispostaPrenotazioneRitiro");
try {
    $server->handle();
}
catch (Exception $e) {
    die("SOAP Fault:<br />fault code: {$fault->faultcode}, fault string: {$fault->faultstring}");
}
?>