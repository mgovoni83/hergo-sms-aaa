<?php
ini_set("auto_detect_line_endings", true);
include_once('/server/prj2/hera_intranet/rifiutologo_wsdl/Classes/PHPExcel/IOFactory.php');
$codici_lingua = array("it" => 1, "en" => 2);
require_once("pwd/SUPER_USER_CONNECT.php");
require_once("pwd/Hera.php");
require_once("super_connect.php");
require_once("sms/lib-mobytsms.inc.php");
if(!class_exists('soapclient'))
    require_once("sms/lib-nusoap.inc.php");
require_once("adm/sms_tracker.php");
require_once("/server/prj2/hr_adm_r13/variabili_di_progetto.php");

$GLOBALS[SESSION_DEBUG]=0;
start_netbox_session();
$debug = 0;	
global $database;
$database = "hera_aaa_sms_rifiuti";
$udbh = super_connect(array(host => $GLOBALS[CONNECTION_HOST], super_user => 1));

$retCode = $details = 'CRON REPORT';

$start_yesterday_time = strtotime("yesterday midnight");
$end_yesterday_time = strtotime("today midnight") - 1;
$yesterday = date("Y.m.d",$start_yesterday_time);

$query = "SELECT idrichiedente, email, descrizione, bcc FROM $database.sms_richiedenti WHERE sms_richiedenti.report_frequency LIKE '%d%' AND email <> ''";
$richiedenti = query(array(DBH => $udbh, sql => $query, direct => 1, debug => $debug, status => 1));
foreach($richiedenti as $richiedente){
    $queryStatus = "SELECT DISTINCT mobyt_status as mobyt_status FROM $database.sms "
                 . "WHERE system_status = 2 AND mobyt_status IS NOT NULL AND idrichiedente = ". $richiedente['idrichiedente'];
    $rowsStatus = query(array(DBH => $udbh, sql => $queryStatus, direct => 1, debug => $debug, status => 1));
    foreach($rowsStatus as $rowStatus){
        $queryReport = "SELECT COUNT(*) AS status_counter FROM $database.sms "
                     . "WHERE (system_created BETWEEN FROM_UNIXTIME($start_yesterday_time) AND FROM_UNIXTIME($end_yesterday_time)) "
                     . "AND mobyt_status = '". $rowStatus['mobyt_status'] ."' AND idrichiedente = ". $richiedente['idrichiedente'];
        $rows = query(array(DBH => $udbh, sql => $queryReport, direct => 1, debug => $debug, status => 1));
        foreach($rows as $row){
            $csv[$richiedente['idrichiedente']]['data'][] = array($rowStatus['mobyt_status'],$row['status_counter']);
            $csv[$richiedente['idrichiedente']]['email'] = $richiedente['email'];
            $csv[$richiedente['idrichiedente']]['timeslot'] = "Report giornaliero ($yesterday)";
            $csv[$richiedente['idrichiedente']]['type'] = 'd';
            $csv[$richiedente['idrichiedente']]['filename'] = date("Ymd",$start_yesterday_time).'-status';
            $csv[$richiedente['idrichiedente']]['descrizione'] = $richiedente['descrizione'];
            $csv[$richiedente['idrichiedente']]['bcc'] = $richiedente['bcc'];
        }
    }
    $queryReportNull = "SELECT COUNT(*) AS status_counter FROM $database.sms "
                     . "WHERE (system_created BETWEEN FROM_UNIXTIME($start_yesterday_time) AND FROM_UNIXTIME($end_yesterday_time)) "
                     . "AND mobyt_status IS NULL AND idrichiedente = ". $richiedente['idrichiedente'];
    $rowsNull = query(array(DBH => $udbh, sql => $queryReportNull, direct => 1, debug => $debug, status => 1));
    foreach($rowsNull as $rowNull){
        $csv[$richiedente['idrichiedente']]['data'][] = array('Esito sconosciuto',$rowNull['status_counter']);
        $csv[$richiedente['idrichiedente']]['email'] = $richiedente['email'];
        $csv[$richiedente['idrichiedente']]['timeslot'] = "Report giornaliero ($yesterday)";
        $csv[$richiedente['idrichiedente']]['type'] = 'd';
        $csv[$richiedente['idrichiedente']]['filename'] = date("Ymd",$start_yesterday_time).'-status';
        $csv[$richiedente['idrichiedente']]['descrizione'] = $richiedente['descrizione'];
        $csv[$richiedente['idrichiedente']]['bcc'] = $richiedente['bcc'];
    }
    $queryReportQueue = "SELECT COUNT(*) AS status_counter FROM $database.sms "
                 . "WHERE (system_created BETWEEN FROM_UNIXTIME($start_yesterday_time) AND FROM_UNIXTIME($end_yesterday_time)) "
                 . "AND system_status = 1 AND idrichiedente = ". $richiedente['idrichiedente'];
    $rowsQueue = query(array(DBH => $udbh, sql => $queryReportQueue, direct => 1, debug => $debug, status => 1));
    foreach($rowsQueue as $rowQueue){
        $csv[$richiedente['idrichiedente']]['data'][] = array('In coda',$rowQueue['status_counter']);
        $csv[$richiedente['idrichiedente']]['email'] = $richiedente['email'];
        $csv[$richiedente['idrichiedente']]['timeslot'] = "Report giornaliero ($yesterday)";
        $csv[$richiedente['idrichiedente']]['type'] = 'd';
        $csv[$richiedente['idrichiedente']]['filename'] = date("Ymd",$start_yesterday_time).'-status';
        $csv[$richiedente['idrichiedente']]['descrizione'] = $richiedente['descrizione'];
        $csv[$richiedente['idrichiedente']]['bcc'] = $richiedente['bcc'];
    }
    $queryReportFail = "SELECT COUNT(*) AS status_counter FROM $database.sms "
                 . "WHERE (system_created BETWEEN FROM_UNIXTIME($start_yesterday_time) AND FROM_UNIXTIME($end_yesterday_time)) "
                 . "AND system_status = 0 AND idrichiedente = ". $richiedente['idrichiedente'];
    $rowsFail = query(array(DBH => $udbh, sql => $queryReportFail, direct => 1, debug => $debug, status => 1));
    foreach($rowsFail as $rowFail){
        $csv[$richiedente['idrichiedente']]['data'][] = array('Invio fallito',$rowFail['status_counter']);
        $csv[$richiedente['idrichiedente']]['email'] = $richiedente['email'];
        $csv[$richiedente['idrichiedente']]['timeslot'] = "Report giornaliero ($yesterday)";
        $csv[$richiedente['idrichiedente']]['type'] = 'd';
        $csv[$richiedente['idrichiedente']]['filename'] = date("Ymd",$start_yesterday_time).'-status';
        $csv[$richiedente['idrichiedente']]['descrizione'] = $richiedente['descrizione'];
        $csv[$richiedente['idrichiedente']]['bcc'] = $richiedente['bcc'];
    }
}

foreach($csv as $idrichiedente => $reportFile){
    $dir = 'csv/'.$reportFile['type'].'/'.$idrichiedente.'/';
    if (!file_exists($dir))
        mkdir($dir, 0755, true);
    $fp = fopen($dir.$reportFile['filename'].'.csv', 'w+');
    fputcsv($fp, array('stato','contatore'));
    foreach ($reportFile['data'] as $fields)
        fputcsv($fp, $fields);
    fclose($fp);

    $objReader = PHPExcel_IOFactory::createReader('CSV');
    $objReader->setDelimiter(",");
    $objReader->setInputEncoding('UTF-8');
    $objReader->setReadDataOnly(false);
    $objPHPExcel = $objReader->load($dir.$reportFile['filename'].'.csv');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $dir = 'xls/'.$reportFile['type'].'/'.$idrichiedente.'/';
    $objWriter->save($dir.$reportFile['filename'] .'.xls');

    $filename = str_replace('/nfs/gruppohera','/server', dirname(__FILE__)) .'/'. $dir . $reportFile['filename'];
    $attachments = array($reportFile['filename'] => $filename.'.xls');

    $subject = $reportFile['timeslot'];
    $msg = 'Report da Officine Digitali per l\'invio SMS del servizio '.$reportFile['descrizione'].'.';
    mail64($reportFile['email'], $subject, $msg, 'Reportistica Invio SMS <noreply@gruppohera.it>', 1, 1, $attachments, $reportFile['bcc']);
}