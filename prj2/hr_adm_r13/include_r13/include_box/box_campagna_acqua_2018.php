
<!--googleon: index-->
<div class="box_layout campagna_acqua_2018 boxHeightSmall <?= $box['css'] ?> <? if($box['css_width'] != ''): echo $box['css_width']; else: ?>row-fluid<? endif; ?>" <? $OD->showDevelBoxAttr($box); ?>>    
    <link href="http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/video-js.min.css" rel="stylesheet" />
    <style>
        .campagna_acqua_2018,
        .video-js-container{
            background-color: #AECAE2;
            color: #143d69;
            line-height: 125%
        }
        .video-js-container{
            height: 100%;
            position: relative;
        }
        .hider{
            display: none
        }
        .vjs-loading-spinner{
            display: none!important
        }
        .main-preview-controls{
            background-color: rgba(0,0,0,0);
            z-index: 99;
            width: 100%;
            position: absolute
        }
        .main-preview-controls,
        .main-preview-player {
            max-width: 1920px;
            max-height: 596px;
        }
        .filler,
        .video-js{
          position: relative;
          min-width: 300px;
          min-height: 150px;
          height: 0;
        }
        .main-preview-controls span.control{
            display: block;
            float: left;
            cursor: pointer
        }
        .control1{
            width:11.5%;
        }
        .control2{
            width:10%
        }
        .control3{
            width:8.5%
        }
        .control4{
            width:13.5%
        }
        .control5{
            width:9%
        }
        .control6{
            width:11%
        }
        .control7{
            width:9%
        }
        .main-preview-controls .finisher{
            width:32.5%;
            float: right;
        }
        div#heraVideo:before {
            content: '';
            display: block;
            right: 0;
            width: 2px;
            position: absolute;
            background-color: #aecae2;
            height: 100%;
            top: 0;
            z-index: 1;
        }
        div#heraVideo:after {
            content: '';
            display: block;
            left: 0;
            width: 1px;
            position: absolute;
            background-color: #aecae2;
            height: 100%;
            top: 0;
            z-index: 1;
        }
        .campagna_acqua_2018 .title{
            font-size: 45px;
            line-height: 50px;
            font-weight: 700;
            text-transform: uppercase;
            margin: 75px 0;
        }
        .campagna_acqua_2018 .video-title{
            font-size: 20px;
            font-weight: 700;
            margin: 25px 0
        }
        .campagna_acqua_2018 .sub-title{
            font-size: 21px;
            font-weight: 700;
            line-height: 30px;
            margin: 75px 0 25px;
        }
        .campagna_acqua_2018 .paragraph{
            font-size: 15px;
            line-height: 22px;
            padding: 0 8px
        }
        .campagna_acqua_2018 .paragraph-bold{
            font-size: 19px
        }
        .campagna_acqua_2018 .margined-bottom{
            margin-bottom: 125px
        }
        .campagna_acqua_2018 .caps-lock-title{
            text-transform: uppercase;
            font-size: 21px;
            font-weight: 700;
            margin: 25px 0
        }
        .fumetto a:hover{
            text-decoration:none
        }
        .fumetto a {
            min-width: 202px;
            display: inline-block;
            min-height: 56px;
            text-align: center
        }
        .fumetto1 .fumetto a {
            background: url(http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/goccia.png) no-repeat 25px center;
        }
        .fumetto{
            background: url('http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/pozzanghera.png') no-repeat center;
            min-height: 56px;
            display: inline-table;
        }
        .fumetto span{
            color: #fff;
            font-weight: 700;
            line-height: 56px;
            font-size: 13px;            
        }
        .fumetto1 .fumetto span{
            margin-top: 8px;
            display: inline-block;
            line-height: 20px;
            width:100px;
        }
        .campagna_acqua_2018 .vjs-poster{
            display: inline-table
        }
        .campagna_acqua_2018 .video-js{
            background-color: #AECAE2
        }
        @media only screen and (max-width: 1200px){
            .campagna_acqua_2018 .title{
                margin: 55px 0;
                font-size: 40px;
                line-height: 45px;
            }
            .campagna_acqua_2018 .video-title{
                font-size: 18px;
                margin: 22.5px 0
            }
            .campagna_acqua_2018 .sub-title{
                font-size: 18px;
                line-height: 28px;
                margin: 55px 0 25px;
            }
            .campagna_acqua_2018 .caps-lock-title{
                font-size: 18px;
                margin: 22.5px 0
            }
            .campagna_acqua_2018 .margined-bottom{
                margin-bottom: 100px
            }
        }
        @media only screen and (max-width: 768px){
            .fumetto1,.fumetto2,.fumetto3,.fumetto4{
                text-align: center!important;
                margin: 15px 0
            }
            .campagna_acqua_2018 .title{
                margin: 35px 0;
                font-size: 30px;
                line-height: 35px
            }
            .campagna_acqua_2018 .video-title{
                font-size: 16px;
                margin: 20px 0
            }
            .campagna_acqua_2018 .sub-title{
                font-size: 15px;
                line-height: 25px;
                margin: 35px 0 25px;
            }
            .campagna_acqua_2018 .caps-lock-title{
                font-size: 15px;
                margin: 20px 0
            }
            .campagna_acqua_2018 .margined-bottom{
                margin-bottom: 75px
            }
        }
        @media only screen and (max-width: 400px){
            .header{
                height:100px;
            }
            .campagna_acqua_2018 .title{
                margin: 15px 0;
                font-size: 20px;
                line-height: 25px
            }
            .campagna_acqua_2018 .video-title{
                font-size: 14px;
                margin: 17.5px 0
            }
            .campagna_acqua_2018 .sub-title{
                font-size: 12px;
                line-height: 22px;
                margin: 15px 0 25px;
            }
            .campagna_acqua_2018 .caps-lock-title{
                font-size: 12px;
                margin: 17.5px 0
            }
            .campagna_acqua_2018 .margined-bottom{
                margin-bottom: 50px
            }
        }
        /*
        .frame{
            width:300px
        }
        .slidee{
            width:1000px
        }
        */
    </style>
    <div class="row-fluid">
        <div class="center">
            <div class="title">scopri il valore dell'acqua di casa tua</div>
            <div class="video-title">Clicca su di noi per ascoltare le nostre storie.</div>
        </div>
    </div>
    <div class="row-fluid frame">
        <div class="no-gutters video-js-container slidee">
            <div class="col main-preview-controls">
                <span class="control control1" data-start="0" data-stop="17"></span>
                <span class="control control2" data-start="17" data-stop="26.65"></span>
                <span class="control control3" data-start="26.65" data-stop="47.05"></span>
                <span class="control control4" data-start="47.05" data-stop="63.4"></span>
                <span class="control control5" data-start="63.4" data-stop="79.75"></span>
                <span class="control control6" data-start="79.75" data-stop="96.9"></span>
                <span class="control control7" data-start="96.9" data-stop="116.75"></span>
                <span class="finisher"></span>
            </div>
            <div class="col main-preview-player">
                <div class="filler"></div>
                <video id="heraVideo" class="hider video-js vjs-default-skin vjs-fluid" data-setup='{"controls":false,"autoplay":false,"preload":"auto","loop":false,"poster":"http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/working.png"}'></video>
                <a class="bplan hider" target="_blank" href="https://www.youtube.com/watch?v=c1qjGShodWs" rel="prettyPhoto"><img src="http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/working.png" /></a>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="center">
            <div class="sub-title">Dietro il tuo rubinetto c'&egrave; un mondo di persone<br/>che si impegnano per garantirti un'acqua sicura, sana e a km 0.</div>
            <div class="caps-lock-title">cinque buoni motivi per bere l'acqua del rubinetto</div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="offset1 span2">
            <div class="paragraph"><b>SICURA:</b> Hera la controlla oltre <b>2.000 volte al giorno</b>;</div>
            <br/>
            <div class="paragraph"><b>VICINA:</b> &egrave; a <b> km 0</b> perch&egrave; nasce dalle fonti del territorio e arriva direttamente a casa tua;</div>
        </div>
        <div class="offset1 span3">
            <div class="paragraph"><b>SANA:</b> &egrave; <b>oligominerale</b> e a basso contenuto di sodio;</div>
            <br/>
            <div class="paragraph"><b>ECOLOGICA:</b> il 36% dei cittadini che l'ha scelta ha consentito, nel 2017, di risparmiare <b>250 milioni</b> di bottiglie di plastica;</div>
        </div>
        <div class="offset1 span3">
            <div class="paragraph"><b>ECONOMICA:</b> ti fa risparmiare 300 euro l'anno rispetto all'acqua in bottiglia;</div>
            <br/>
            <div class="paragraph paragraph-bold">L'acqua Hera &egrave; <b>SOSTENIBILE</b>.</div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="center">
            <div class="sub-title">Essere sempre aggiornato sulla qualit&agrave; dell'acqua del rubinetto &egrave; facilissimo:</div>
        </div>
    </div>
    <div class="row-fluid margined-bottom center">
        <?php /*
        <div class="fumetto1 text-right span4"><div class="fumetto"><a href="http://www.gruppohera.it/clienti/casa/acquologo/"><span>Scarica l'app Acquologo.it</span></a></div></div>
        <div class="fumetto2 text-center span4"><div class="fumetto"><a href="http://www.gruppohera.it/gruppo/attivita_servizi/business_acqua/canale_acqua/report_buone_acque/"><span>Report "IN BUONE ACQUE"</span></a></div></div>
        <div class="fumetto3 text-left span4"><div class="fumetto"><a href="http://www.gruppohera.it/gruppo/attivita_servizi/business_acqua/canale_acqua/"><span>Area web dedicata</span></a></div></div>
        */ ?>
        <div class="fumetto1 text-right span3"><div class="fumetto"><a href="http://www.gruppohera.it/clienti/casa/acquologo/"><span>Scarica l'app Acquologo.it</span></a></div></div>
        <div class="fumetto2 text-center span3"><div class="fumetto"><a href="http://www.gruppohera.it/gruppo/attivita_servizi/business_acqua/canale_acqua/report_buone_acque/"><span>Report "IN BUONE ACQUE"</span></a></div></div>
        <div class="fumetto3 text-center span3"><div class="fumetto"><a href="http://www.gruppohera.it/gruppo/attivita_servizi/business_acqua/canale_acqua/"><span>Area web dedicata</span></a></div></div>
        <div class="fumetto4 text-left span3"><div class="fumetto"><a href="javascript: void(0)"><span>Ascolta lo spot radiofonico</span></a></div></div>
        <audio id="valore_acqua"><source src="http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/spot_valoreacqua.mp3" type="audio/mpeg"></audio>
    </div>
    <script src="http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/video.min.js"></script>
    <script src="http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/sly.min.js"></script>
    <script>
        var player = videojs('heraVideo');
        function checkLoad() {
            if (player.readyState() === 4) {
                jQuery('.main-preview-player .bplan').addClass('hider');
                jQuery('.main-preview-player .filler,.main-preview-controls').removeClass('hider');
                jQuery('.filler').fadeOut('slow', function(){
                    jQuery('.video-js').fadeIn('slow', function(){
                        resizeWrapper();
                        var t;
                        jQuery('.main-preview-controls span.control').click(function(){
                            clearTimeout(t);
                            start = jQuery(this).attr('data-start');
                            stop = jQuery(this).attr('data-stop');
                            player.pause();
                            if(player.currentTime() > start && player.currentTime() < stop)
                                player.currentTime(0);
                            else{
                                delay = (stop-start) * 1000;
                                player.currentTime(start);
                                player.play();
                                t = setTimeout(function(){
                                    player.pause();
                                    player.currentTime(0);
                                }, delay);
                            }
                        });
                    });
                });
            }
            else{
                if(jQuery('.main-preview-player .bplan').hasClass('hider')){
                    jQuery('.main-preview-player .bplan').removeClass('hider');
                    jQuery('.main-preview-player .filler,.main-preview-controls').addClass('hider');
                }
                setTimeout(checkLoad, 100);
            }
        }
        function resizeWrapper(){
            jQuery('.video-js-container .main-preview-controls span').height(jQuery('.video-js-container .main-preview-player video').height());
        }
        jQuery(function(){
            jQuery('.fumetto4 a').click(function(){
                var audio = document.getElementById('valore_acqua');
                audio.play();
            });
            jQuery("a[rel^='prettyPhoto']").prettyPhoto();
            jQuery('video#heraVideo').ready(function(){
//                var frame = jQuery('.frame');
//                frame.sly({horizontal: 1, itemNav: 0, mouseDragging: 1, touchDragging: 1,});
                player.src({type: "video/mp4", src: "http://www.gruppohera.it/binary/hr_acqua/campagna_acqua_2018/land.mp4"});
                checkLoad();
                jQuery(window).resize(function(){
                    resizeWrapper();
//                    sly.reload();
                });
            });
        });
    </script>
</div>
<!--googleoff: index-->