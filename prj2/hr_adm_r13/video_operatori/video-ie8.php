<html>
    <head>
        <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="//vjs.zencdn.net/ie8/1.1.1/videojs-ie8.js"></script>
        <script src="//vjs.zencdn.net/5.5.3/video.js"></script>
        <link href="//vjs.zencdn.net/5.5.3/video-js.min.css" rel="stylesheet" />
        <style>
            body{
                background-color: #fff
            }
            .header{
                height:150px;
                display: block;
                background-color: #AECAE2;
            }
            .video-js-container{
                background-color: #AECAE2;
                height: 100%;
                position: relative;
            }
            .hider{
                display: none
            }
            .vjs-loading-spinner{
                display: none!important
            }
            .main-preview-controls{
                background-image: url('assets/pixel.png');
                z-index: 99;
                position: absolute
            }
            .main-preview-controls,
            .main-preview-player {
                max-width: 1920px;
                max-height: 596px;
            }
            .filler,
            .video-js{
              position: relative;
              min-width: 300px;
              min-height: 150px;
              height: 0;
            }
            .main-preview-controls span.control{
                display: block;
                float: left;
                cursor: pointer
            }
            .main-preview-controls .finisher{
                width: 32.5%;
                float: right;
            }
            div#heraVideo:before {
                content: '.';
                display: block;
                right: 0;
                width: 2px;
                position: absolute;
                background-color: #aecae2;
                height: 100%;
                top: 0;
                z-index: 1;
            }
            div#heraVideo:after {
                content: '.';
                display: block;
                left: 0;
                width: 1px;
                position: absolute;
                background-color: #aecae2;
                height: 100%;
                top: 0;
                z-index: 1;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row no-gutters header">
            </div>
            <div class="row no-gutters video-js-container">
                <div class="col main-preview-controls">
                    <span class="control control1" data-w="11.5" data-start="0" data-stop="16.1"></span>
                    <span class="control control2" data-w="10" data-start="16.1" data-stop="25.75"></span>
                    <span class="control control3" data-w="8.5" data-start="25.75" data-stop="46.7"></span>
                    <span class="control control4" data-w="10.5" data-start="46.7" data-stop="63.1"></span>
                    <span class="control control5" data-w="9" data-start="63.1" data-stop="79.79"></span>
                    <span class="control control6" data-w="9" data-start="79.79" data-stop="96"></span>
                    <span class="control control7" data-w="9" data-start="96" data-stop="160"></span>
                    <span class="finisher"></span>
                </div>
                <div class="col main-preview-player">
                    <div class="filler"></div>
                </div>
            </div>
        </div>
        <script>
            jQuery('.main-preview-player').append('<video id="heraVideo" class="hider video-js vjs-default-skin vjs-fluid" data-setup=\'{"autoplay":false,"preload":"auto","loop":false}\'><source src="assets/land.mp4" type="video/mp4" /></video>');
            var player = videojs('heraVideo');
            function checkLoad() {
                jQuery('.filler').fadeOut('slow', function(){
                    jQuery('.video-js').fadeIn('slow', function(){
                        resizeWrapper();
                        var t;
                        jQuery('.main-preview-controls span.control').click(function(){
                            clearTimeout(t);
                            start = jQuery(this).attr('data-start');
                            stop = jQuery(this).attr('data-stop');
                            player.pause();
                            if(player.currentTime() > start && player.currentTime() < stop)
                                player.currentTime(0);
                            else{
                                delay = (stop-start) * 1000;
                                player.currentTime(start);
                                player.play();
                                t = setTimeout(function(){
                                    player.pause();
                                    player.currentTime(0);
                                }, delay);
                            }
                        });
                    });
                });
            }
            function resizeWrapper(){
                var spanContainerW = jQuery('.video-js-container .main-preview-player').width();
                var spanContainerH = jQuery('.video-js-container .main-preview-player').height();
                jQuery('.main-preview-controls span.control').each(function(){
                    var dataW = jQuery(this).attr('data-w');
                    console.log(dataW);
                    console.log(spanContainerW);
                    console.log(dataW * spanContainerW / 100);
                    jQuery(this).width(dataW * spanContainerW / 100).height(spanContainerH);
                });
                jQuery('.video-js-container .main-preview-controls span').height(spanContainerH);
            }
            jQuery(function(){
                jQuery('video#heraVideo').ready(function(){
                    checkLoad();
                    jQuery(window).resize(function(){
                        resizeWrapper();
                    });
                });
            });
        </script>
    </body>
</html>