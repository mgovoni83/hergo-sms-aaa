<?php
require_once('../variabili_di_progetto.php');

function convert($size){
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

/** Error reporting */
error_reporting(E_ERROR|E_WARNING);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/Rome');

set_time_limit ( 10 * 30 ); // 10 minuti

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

require_once 'Classes/PHPExcel.php';



$campi = array(
    # CHIEDERE A ZONARELLI
    array('label'=>"teleseller", value=>"AGWEB"), # array('label'=>"teleseller", value=>"WEB"),
    # CHIEDERE A ZONARELLI
    array('label'=>"Id commerciale", value=>"AGWEBFORM"), # array('label'=>"Id commerciale", value=>"agsme"),
    # DA RIGUARDARE
    array('label'=>"pk"),
    # CHIEDERE A ZONARELLI?
    array('label'=>"CAMPAGNA", value=>"WEBFORM"), # array('label'=>"CAMPAGNA),
    # OK
    array('label'=>"LISTA"),
    # CHIEDERE A ZONARELLI?
    array('label'=>"TIPO CANALE ACQUISIZIONE", value=>"WEB"), # array('label'=>"TIPO CANALE ACQUISIZIONE", value=>"WEB SELF CARE"),
    # CHIEDERE A ZONARELLI?
    array('label'=>"AZIONE COMMERCIALE", value=>"WEBFORM"), # array('label'=>"AZIONE COMMERCIALE", valueif=>"CAMPAGNA COOP"),
    # OK
    array('label'=>"DATA OGGI"), # array('label'=>"DATA OGGI", field=>'system_modified'),
    # OK
    array('label'=>"ID PROGRESSIVO"),
    # OK
    array('label'=>"DATA VO", field=>'system_modified'),
// 10
    # OK
    array('label'=>"FILE AUDIO"),
    # OK
    array('label'=>"NOME INTERLOCUTORE"),
    # OK
    array('label'=>"COGNOME INTERLOCUTORE"),
    # CHIEDERE A ZONARELLI?
    array('label'=>"Fonte privacy", value=>'WEB'),
    # ELENCO CAMPI
    array('label'=>"PRIVACY", value=>'SI'),
    # OK
    array('label'=>"COD.CLIENTE", field=>'vtff_codcliente'),
    # OK
    array('label'=>"COGNOME_LR"),
    # OK
    array('label'=>"NOME_LR"),
    # OK
    array('label'=>"CODICE FISCALE_LR"),
    # OK
    array('label'=>"CANALE PROVENIENZA"),
// 20
    # OK
    array('label'=>"ALTRO CANALE PROVENIENZA"),
    # OK
    array('label'=>"TIPO PROMOZIONE"), # array('label'=>"TIPO PROMOZIONE", valueif=>"promo COOP Adriatica"),
    # OK
    array('label'=>"CODICE SOCIO"), # array('label'=>"CODICE SOCIO", field=>"vtff_coop_codice"),
    # OK
    array('label'=>"NOME SOCIO"), # array('label'=>"NOME SOCIO", field=>"vtff_coop_nome"),
    # OK
    array('label'=>"COGNOME SOCIO"), # array('label'=>"COGNOME SOCIO", field=>"vtff_coop_cognome"),
    # OK
    array('label'=>"INDIRIZZO sede legale"),
    # OK
    array('label'=>"CIVICO sede legale"),
    # OK
    array('label'=>"CAP sede legale"),
    # OK
    array('label'=>"COMUNE sede legale"),
    # OK
    array('label'=>"PROVINCIA sede legale"),
// 30
    # OK
    array('label'=>"PIVA"),
    # OK
    array('label'=>"ASSOCIAZIONE"),
    # OK
    array('label'=>"COGNOME", field=>'vtff_cognome'),
    # OK
    array('label'=>"NOME", field=>'vtff_nome'),
    # OK
    array('label'=>"CODICE FISCALE", field=>'vtff_codice_fiscale'),
    # DA VERIFICARE
    array('label'=>"INDIRIZZO RESIDENZA", field=>'vtff_residenza_indirizzo', fielddef=>'vtff_fornitura_indirizzo'),
    # DA VERIFICARE
    array('label'=>"CIVICO RESIDENZA", field=>'vtff_residenza_num', fielddef=>'vtff_fornitura_num'),
    # DA VERIFICARE
    array('label'=>"CAP RESIDENZA", field=>'vtff_residenza_cap', fielddef=>'vtff_fornitura_cap'),
    # DA VERIFICARE
    array('label'=>"COMUNE RESIDENZA", field=>'vtff_residenza_comune', fielddef=>'vtff_fornitura_comune'),
    # DA VERIFICARE
    array('label'=>"PROVINCIA RESIDENZA", field=>'vtff_residenza_prov', fielddef=>'vtff_fornitura_prov'),
// 40
    # DA VERIFICARE
    array('label'=>"INDIRIZZO SPEDIZIONE", field=>'vtff_comunicazioni_indirizzo', fielddef=>'vtff_fornitura_indirizzo'),
    # DA VERIFICARE
    array('label'=>"CIVICO SPEDIZIONE", field=>'vtff_comunicazioni_num', fielddef=>'vtff_fornitura_num'),
    # DA VERIFICARE
    array('label'=>"CAP SPEDIZIONE", field=>'vtff_comunicazioni_cap', fielddef=>'vtff_fornitura_cap'),
    # DA VERIFICARE
    array('label'=>"COMUNE SPEDIZIONE", field=>'vtff_comunicazioni_comune', fielddef=>'vtff_fornitura_comune'),
    # DA VERIFICARE
    array('label'=>"PROVINCIA SPEDIZIONE", field=>'vtff_comunicazioni_prov', fielddef=>'vtff_fornitura_prov'),
    # SEMBRA OK, MA MESSAGGI DISCORDANTI
    array('label'=>"C/O NOMINATIVO RECAPITO"),
    # CONTROLLO SULL'ESISTENZA DI UNO DEI 2 CAMPI SUCCESSIVI?
    # OK
    array('label'=>"TELEFONO FISSO", field=>'vtff_telefono'),
    # OK
    array('label'=>"CELLULARE", field=>'vtff_cellulare'),
    # OK
    array('label'=>"EMAIL", field=>'vtff_email'),
    # OK
    array('label'=>"TIPOLOGIA PAGAMENTO", value=>'RID'), # array('label'=>"TIPOLOGIA PAGAMENTO", field=>'vtff_q_modalita_pagamento'),	// RID o Bollettino postale
// 50
    # OK
    array('label'=>"IBAN IT", field=>'vtff_cc_iban'),
    # OK
    array('label'=>"NOME TITOLARE IBAN", field=>'vtff_cc_nome', fielddef=>'vtff_nome'),
    # OK
    array('label'=>"COGNOME TITOLARE IBAN", field=>'vtff_cc_cognome', fielddef=>'vtff_cognome'),
    # OK
    array('label'=>"CF TITOLARE IBAN", field=>'vtff_cc_codice_fiscale', fielddef=>'vtff_codice_fiscale'),
    # ELENCO CAMPI
    array('label'=>"SETTORE MERCEOLOGICO"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"cognome_ENERGIA", field=>'vtff_ee_cognome', fielddef=>'vtff_cognome'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"nome_ENERGIA", field=>'vtff_ee_nome', fielddef=>'vtff_nome'),
    # OK
    array('label'=>"RAGIONE SOCIALE PRESENTE IN BOLLETTA_EE"),
    # OK
    array('label'=>"PIVA PRESENTE IN BOLLETTA_EE"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"CF BOLLETTA ENERGIA", field=>'vtff_ee_codice_fiscale', fielddef=>'vtff_codice_fiscale'),
// 60
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"Voltura ee"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"COMUNE FORNITURA_EE", field=>'vtff_fornitura_comune', notoftype=>"_gas_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"INDIRIZZO FORNITURA_EE", field=>'vtff_fornitura_indirizzo', notoftype=>"_gas_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"CIVICO_EE", field=>'vtff_fornitura_num', notoftype=>"_gas_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"CAP_EE", field=>'vtff_fornitura_cap', notoftype=>"_gas_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, DA VERIFICARE
    array('label'=>"PROVINCIA_EE", field=>'vtff_fornitura_prov', notoftype=>"_gas_"),
    # ELENCO CAMPI
    array('label'=>"indirizzo res = indirizzo forn", field=>'vtff_q_indirizzo_residenza'),
    # RIGA RIMOSSA
    # array('label'=>"OFFERTA_IMOLA"),
    # OK
    array('label'=>"OFFERTA COMMERCIALE EE"),
    # OK
    array('label'=>"VERSIONE_EE"),
// 70
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, ELENCO CAMPI
    array('label'=>"BONUS_EE"),
    # OK
    array('label'=>"TIPO OFFERTA_EE"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, ELENCO CAMPI
    array('label'=>"PROFILO EE", field=>'vtff_ee_profilo'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, ELENCO CAMPI
    array('label'=>"EE_SPESA BIMESTRALE E/O CONSUMO ANNUO", field=>'vtff_ee_consumo_annuo_eff'),
    # OK
    array('label'=>"NOME ATTUALE FORNITORE_EE"), # array('label'=>"NOME ATTUALE FORNITORE_EE", field=>'vtff_ee_attuale_fornitore'),
    # OK
    array('label'=>"CONTRIBUTO_EE"), # array('label'=>"CONTRIBUTO_EE", field=>'vtff_ee_contributo_mensile'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, ELENCO CAMPI
    array('label'=>"ATTUALE FORNITORE EE"),
    # OK
    array('label'=>"TENSIONE"),
    # OK
    array('label'=>"TIPO CONTATORE"),
    # OK
    array('label'=>"IVA"),
// 80
    # OK
    array('label'=>"POTENZA IMPEGNATA AZIENDA_EE"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, OK
    array('label'=>"POD", field=>'vtff_ee_pod'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = ENERGIA ELETTRICA, ELENCO CAMPI
    array('label'=>"POTENZA", field=>'vtff_ee_pot_impegnata'),
    # OK
    array('label'=>"DATA INIZIO EE"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"cognome_GAS", field=>'vtff_gas_cognome', fielddef=>'vtff_cognome'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"nome_GAS", field=>'vtff_gas_nome', fielddef=>'vtff_nome'),
    # OK
    array('label'=>"RAGIONE SOCIALE PRESENTE IN BOLLETTA_GAS"),
    # OK
    array('label'=>"PIVA PRESENTE IN BOLLETTA_GAS"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"CF BOLLETTA GAS", field=>'vtff_gas_codice_fiscale', fielddef=>'vtff_codice_fiscale'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, ELENCO CAMPI
    array('label'=>"VOLTURA GAS"),
// 90
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"INDIRIZZO FORNITURA_GAS", field=>'vtff_fornitura_indirizzo', notoftype=>"_ee_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"CIVICO_GAS", field=>'vtff_fornitura_num', notoftype=>"_ee_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"CAP_GAS", field=>'vtff_fornitura_cap', notoftype=>"_ee_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"COMUNE_GAS", field=>'vtff_fornitura_comune', notoftype=>"_ee_"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, DA VERIFICARE
    array('label'=>"PROVINCIA_GAS", field=>'vtff_fornitura_prov', notoftype=>"_ee_"),
    # OK
    array('label'=>"HC MARCHE"),
    # OK
    array('label'=>"OFFERTA COMMERCIALE GAS"),
    # OK
    array('label'=>"VERSIONE_GAS"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, ELENCO CAMPI
    array('label'=>"BONUS_GAS"),
    # OK
    array('label'=>"TIPO OFFERTA_GAS"),
// 100
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, ELENCO CAMPI
    array('label'=>"GAS_SPESA BIMESTRALE E/O CONSUMO ANNUO", field=>'vtff_gas_consumo_annuo_eff'),
    # OK
    array('label'=>"CONTRIBUTO_GAS"), # array('label'=>"CONTRIBUTO_GAS", field=>'vtff_gas_contributo_mensile'),
    # OK
    array('label'=>"TIPO CONTRATTO GAS"),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, ELENCO CAMPI
    array('label'=>"ATTUALE FORNITORE_GAS"),
    # OK
    array('label'=>"NOME ATTUALE FORNITORE_GAS"), # array('label'=>"NOME ATTUALE FORNITORE_GAS", field=>'vtff_gas_attuale_fornitore'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, OK
    array('label'=>"PDR", field=>'vtff_gas_pdr'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, OK
    array('label'=>"MATRICOLA CONTATORE", field=>'vtff_gas_matricola_cont'),
    # OBBLIGATORIO SE SETTORE MERCEOLOGICO = GAS, ELENCO CAMPI
    array('label'=>"UTILIZZO GAS", field=>'vtff_gas_uso'),
    # OK
    array('label'=>"DATA INIZIO GAS"),
    # ELENCO CAMPI
    array('label'=>"INVIO ELETTRONICO DELLA BOLLETTA"),
// 110
    # OBBLIGATORIO SE INVIO ELETTRONICO BOLLETTA = MAIL
    array('label'=>"INDIRIZZO MAIL PER INVIO ELETTRONICO DELLA BOLLETTA", field=>'vtff_email'),
    # NEW FIELD
    array('label'=>"CHIAVE UNIVOCA"),
    # NEW FIELD
    array('label'=>"MAIL"),
    # NEW FIELD
    array('label'=>"MODALITA SPEDIZIONE"),
    # NEW FIELD, OBBLIGATORIO SE OFFERTA = NUOVA IDEA, ELENCO CAMPI
    array('label'=>"NUMERO ABITANTI"),
    # NEW FIELD, OBBLIGATORIO SE OFFERTA = NUOVA IDEA, ELENCO CAMPI
    array('label'=>"SUPERFICIE M2"),
    # NEW FIELD
    array('label'=>"NUMERO ADDETTI"),
    # NEW FIELD
    array('label'=>"SUPERFICIE STABILE M2"),
    # NEW FIELD, OBBLIGATORIO SE OFFERTA = NUOVA IDEA, ELENCO CAMPI
    array('label'=>"PROPRIETA"),
    # NEW FIELD, OBBLIGATORIO SE OFFERTA = NUOVA IDEA, ELENCO CAMPI
    array('label'=>"ABITAZIONE"),
// 120
    # NEW FIELD, ELENCO CAMPI
    array('label'=>"CONSENSO ANALISI PREFERENZE"),
    # NEW FIELD, OBBLIGATORIO SE OFFERTA = NUOVA IDEA, ELENCO CAMPI
    array('label'=>"DURATA_EE"),
    # NEW FIELD, OBBLIGATORIO SE OFFERTA = NUOVA IDEA, ELENCO CAMPI
    array('label'=>"DURATA_GAS"),
    # NEW FIELD, OBBLIGATORIO SE OFFERTA = NUOVA IDEA, ELENCO CAMPI
    array('label'=>"TIPO ATTIVITA SVOLTA"),
    # OK
    array('label'=>"IMPOSTA AGEVOLATA"),
    # OK
    array('label'=>"POD_ENEL"),
    # OK
    array('label'=>"CF"),
    # OK
    array('label'=>"Errore"),
    # OK
    array('label'=>"DettaglioErrore"),
    # OK
    array('label'=>"Esito"),
// 130
    # OK
    array('label'=>"DettaglioEsito"),
    # OK
    array('label'=>"AmmissibilitaCODCAUSALE"),
    # OK
    array('label'=>"AmmissibilitaMOTIVAZIONE"),
    # OK
    array('label'=>"AmmissibilitaVERIFICAAMM"),
    # OK
    array('label'=>"ClienteFinaleCF"),
    # OK
    array('label'=>"ClienteFinalePIVA"),
    # OK
    array('label'=>"DatiTecniciCODPOD"),
    # OK
    array('label'=>"DatiTecniciPOTDISP"),
    # OK
    array('label'=>"DatiTecniciPOTFRANC"),
    # OK
    array('label'=>"DatiTecniciPOTIMP"),
// 140
    # OK
    array('label'=>"DatiTecniciTENSIONE"),
    # OK
    array('label'=>"DatiTecniciMISURATOREELETTRONICOname"),
    # OK
    array('label'=>"DatiTecniciMISURATOREELETTRONICOvalue"),
    # OK
    array('label'=>"DatiTecniciOPZIONETARIFFARIAname"),
    # OK
    array('label'=>"DatiTecniciOPZIONETARIFFARIAvalue"),
    # OK
    array('label'=>"DatiTecniciTIPOLOGIAMISname"),
    # OK
    array('label'=>"DatiTecniciTIPOLOGIAMISvalue"),
    # OK
    array('label'=>"UbicazioneFornituraCAP"),
    # OK
    array('label'=>"UbicazioneFornituraCIV"),
    # OK
    array('label'=>"UbicazioneFornituraINTERNO"),
// 150
    # OK
    array('label'=>"UbicazioneFornituraISTAT"),
    # OK
    array('label'=>"UbicazioneFornituraLOCALITA"),
    # OK
    array('label'=>"UbicazioneFornituraPIANO"),
    # OK
    array('label'=>"UbicazioneFornituraPROV"),
    # OK
    array('label'=>"UbicazioneFornituraSCALA"),
    # OK
    array('label'=>"UbicazioneFornituraTOPONIMO"),
    # OK
    array('label'=>"UbicazioneFornituraVIA"),
    # OK
    array('label'=>"result"),
    # OK
    array('label'=>"errorMessage"),
    # NEW FIELD, ELENCO CAMPI
    array('label'=>"SOCIETA' EROGATRICE"),
// 160
    # NEW FIELD
    array('label'=>"PROVENIENZA", field=>'vtff_fornitura_prov'),
    # NEW FIELD, ELENCO CAMPI
    array('label'=>"NOME OFFERTA"),
    # NEW FIELD, ELENCO CAMPI
    array('label'=>"NOME PRODOTTO"),
    # NEW FIELD, ELENCO CAMPI
    array('label'=>"BONUS1"),
    # NEW FIELD
    array('label'=>"DET_BONUS1"),
    # NEW FIELD
    array('label'=>"BONUS2"),
    # NEW FIELD
    array('label'=>"DET_BONUS2"),
    # NEW FIELD
    array('label'=>"BONUS3"),
    # NEW FIELD
    array('label'=>"DET_BONUS3"),
    # NEW FIELD
    array('label'=>"PROMO"),
// 170
    # NEW FIELD
    array('label'=>"DET_PROMO"),
    # NEW FIELD, DA IMPLEMENTARE
    array('label'=>"VERSIONE"),
    # NEW FIELD
    array('label'=>"NUMERO VAS"),
    # NEW FIELD
    array('label'=>"CHIAVE UNIVOCA CT7"),
// 174
);

// ca_mercato_libero_gas / ca_mercato_libero_ee
$offerte = array(
	"gas" => array("IMOLA"      => array(4,9,15,24,30,42,48,56,64,72,77,80,83,86,89),
                       "MARCHE"     => array(11,12,13,17,18,19,20,21,22,26,27,28,32,33,34,35,36,37,38,39,40,44,45,46,50,51,52,53,54,58,59,60,61,62,66,67,68,69,70,74,75,78,81,84,87,88),
                       "RETE-XR"    => array(1,14,8,25,29,43,47,55,63,57,65,6,7,10,16,23,31,41,49,71,73,76,79,82,85,90)
	),
	"ee" => array("IMOLA"   => array(1,7,12,18,25,34,43,52,61,70,79,84,91,97,104,109,113,117,123,124),
                      "MARCHE"  => array(13,14,15,19,20,21,22,23,24,28,29,31,35,36,37,38,39,40,44,45,46,47,48,49,53,54,55,56,57,62,63,64,65,66,71,72,73,74,75,80,81,85,86,87,92,93,98,99,102,103,110,114,119,127,122),
                      "RETE-XR" => array(4,6,8,9,10,11,16,17,26,27,32,33,41,42,50,51,58,59,60,67,76,68,69,77,78,82,83,88,89,90,94,95,96,100,101,105,106,108,111,112,115,116,118,120,121,125,128)
	)
);

$comuniprov = query(array(direct    =>	1,
                          from      =>	"hera_comm.comuni",
                          status    =>	1,
                          sort      =>	'REF DESC',
                          debug     =>  0
								));
$out = array();
foreach($comuniprov as $c) {
	$out[$c['REF']] = $c;
}
$comuniprov = $out;

$tipofferta = query(array(direct    =>  1,
                          from      =>	"hera_comm.ca_mercato_libero",
                          status    =>	1,
                          sort      =>	'REF DESC',
                          debug     =>	0
								));
$out = array();
foreach($tipofferta as $c) {
	$out[$c['REF']] = $c;
}
$tipofferta = $out;


$callStartTime = microtime(true);

$save_dest = 'download/'.'compilazioni_web.xlsx'; // ############################################	NOME FILE OutPut


$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Hera Comm Web")
							 ->setLastModifiedBy("Hera Comm Web")
							 ->setTitle("Hera Comm Web")
							 ->setSubject("Hera Comm Web")
							 ->setDescription("Hera Comm Web")
							 ->setKeywords("Hera Comm Web")
							 ->setCategory("Hera Comm Web");

$objPHPExcel->setActiveSheetIndex(0);
$excel = $objPHPExcel->setActiveSheetIndex(0);

$riga = '1';
$colonna = 'A';
foreach($campi as $campo) {
	$excel->setCellValue($colonna . $riga, $campo['label']);
	$excel
		->getColumnDimension($colonna)
		->setAutoSize(true);
	$colonna++;
}
$riga++; // qui sarà "2"...

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save($save_dest);

$totaleCompilazioni = query(array(
										direct	=>	1,
										from		=>	"hera_comm.form_adesioni_offerte_casa",
										where		=>	"vtff_form_step = 4",
										status	=>	1,
										sort		=>	'REF DESC',
										debug		=>	0
								));

$compilazioni_totali = count( $totaleCompilazioni );
echo '<br>Totale compilazioni: ' . $compilazioni_totali;

/**/
	$secondi_stimati = 0.03 * $compilazioni_totali;
	echo '<br>Tempo stimato di completamento: '.$secondi_stimati.' secondi';
/**/

ob_flush();

$limit = 300;
$cycleNum = ceil( $compilazioni_totali / $limit );

for($cycle = 0; $cycle < $cycleNum; $cycle++) {
	$offset = $cycle * $limit +1;
	//echo '<br>'.$offset;
	
	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objPHPExcel->load($save_dest); // Empty Sheet
	
	$objPHPExcel->setActiveSheetIndex(0);
	$excel = $objPHPExcel->setActiveSheetIndex(0);
	
	
	$compilazioni = query(array(
											direct	=>	1,
											from		=>	"hera_comm.form_adesioni_offerte_casa",
											where		=>	"vtff_form_step = 4",
											status	=>	1,
											limit		=>	$offset.','.$limit,
											sort		=>	'REF DESC',
											debug		=>	0
									));
	//echo '<hr>';
	
	foreach($compilazioni as $ccc) {
		
		$isDual = false;
		if (		(strtolower( $ccc['vtff_formperofferta'] ) != 'luce' && strtolower( $ccc['vtff_formperofferta'] ) != 'gas')
				|| (trim( $ccc['vtff_offee'] ) != '' && trim( $ccc['vtff_offgas'] ) != '') ) {
			$isDual = true;
		}
		//if ($isDual) echo '. ';
		
		$nome_offerta = $tipofferta[ $ccc['vtff_tipo_offerta'] ]['titolo_it']; 
		
		$compilaz = array($ccc);
		if ($isDual) {
			$ccc_ee = $ccc;
			$ccc_ee['vtff_formperofferta'] = 'Luce';
			$ccc_gas = $ccc;
			$ccc_gas['vtff_formperofferta'] = 'Gas';
			$compilaz = array($ccc_ee, $ccc_gas);
		}
		
		
		foreach($compilaz as $c) { // ciclo per il doppiocampo, nel caso di sottoscrizione di entrambe le offerte
		
			$invio_elettronico_bolletta = false;
		
			$isOfType = 'ee';
			$notOfType = '_gas_'; // in caso di LUCE evito GAS
			if (strtolower( $c['vtff_formperofferta'] ) == 'gas') {
				$isOfType = 'gas';
				$notOfType = '_ee_'; // in caso di GAS evito LUCE
			}
			
			$zona_offerta = null;
			$offerta_commerciale = '';
			if ( trim($c['vtff_off'.$isOfType]) != '') {
				$offREF = $c['vtff_off'.$isOfType];
				foreach($offerte[$isOfType] as $zona => $elencoREF) {
					if (in_array($offREF, $elencoREF) !== false) {
						$zona_offerta = $zona;
						$off_data = query(array(
											direct	=>	1,
											from		=>	"hera_comm.ca_mercato_libero_".$isOfType,
											where		=>	"REF = ".$offREF,
											status	=>	1,
											debug		=>	0
										));
						$offerta_commerciale = $off_data[0]['titolo_it'];
					}
				}
			}
			
			$colonna = 'A';
			foreach($campi as $campo) {
				$valore = '';
				if (isset($campo['value'])) { // ================================================================================================== SE HO SETTATO "value"
					$valore = $campo['value'];
				}
				else { // ========================================================================================================================= ALTRIMENTI...
					if (isset($campo['field'])) { // ----------------------------------------------------------------------------------------------- se è settato "field"
						$canPopulateField = true;
						if (isset($campo['notoftype'])) { // ........................................................................................
							if ($campo['notoftype'] != $notOfType) { // se il campo non va popolato in questo caso (GAS o EE)...
								$canPopulateField = false;
							}
						}
						if ($canPopulateField && strpos($campo['field'], $notOfType) === false) { // controllo sull'offerta della riga attuale, le offerte doppie creano 2 righe!
							if ( strpos($campo['field'], '_comune') !== false || strpos($campo['field'], '_prov') !== false) {
								if (trim( $c[ $campo['field'] ] ) != '' && trim( $c[ $campo['field'] ] ) != 0) {
									$subfield = 'titolo';
									if (strpos($campo['field'], '_prov') !== false) {
										$subfield = 'provincia';
									}
									$valore = $comuniprov[ trim( $c[ $campo['field'] ] ) ][$subfield];
								}
								else {
									$valore = '';
								}
							}
							else {
								$valore = trim( $c[ $campo['field'] ] );
							}
	
							if (trim($valore) == '' && isset($campo['fielddef'])) { // --------------------------------------------------------------- valore di default, se field è vuoto
								if ( strpos($campo['fielddef'], '_comune') !== false || strpos($campo['fielddef'], '_prov') !== false) {
									if (trim( $c[ $campo['fielddef'] ] ) != '' && trim( $c[ $campo['fielddef'] ] ) != 0) {
										$subfield = 'titolo';
										if (strpos($campo['field'], '_prov') !== false) {
											$subfield = 'provincia';
										}
										$valore = $comuniprov[ trim( $c[ $campo['fielddef'] ] ) ][$subfield];
									}
									else {
										$valore = '';
									}
								}
								else {
									$valore = trim( $c[ $campo['fielddef'] ] );
								}
							}
	
							if ( strpos($campo['field'], '_indirizzo') !== false) { // --------------------------------------------------------------- imposto il prefisso dell'indirizzo (solo se presente!)
								$pre = trim( $c[ strtr($campo['field'],array('_indirizzo'=>'_str')) ] );
								if ($pre == '-') {
									$pre = 'via';
								}
								$valore = $pre .' '. $valore;
							}
							
						}
					}
					
					if ( strpos($campo['field'], '_q_modalita_pagamento') !== false) { // ---------------------------------------------------------- normalizzo la stringa sulla modalità di pagamento
						if ($valore == '' && trim($c[ 'vtff_cc_iban' ]) != '') {
							$valore = 'addebito cc';
						}
						if ($valore == 'addebito cc') {
							$valore = 'RID';
						}
						else {
							$valore = 'Bollettino postale';
						}
					}
					
					if (strpos($campo['label'], " IBAN") !== false) { // --------------------------------------------------------------------------- verifico se sono nei campi del blocco CC
						if ( trim($c['vtff_cc_iban']) == '') { // se non ho compilato IBAN, allora i campi annessi vanno vuoti!
							$valore = '';
						}
					}
					
					if (strpos($campo['label'], "AZIONE COMMERCIALE") !== false) { // -------------------------------------------------------------- assegno il valore al campo solo nel caso COOP
						if (isset($c['vtff_coop_codice']) && trim($c['vtff_coop_codice'])!='') {
							$valore = $campo['valueif'];
						}
					}
					if (strpos($campo['label'], "TIPO PROMOZIONE") !== false) { // ----------------------------------------------------------------- assegno il valore al campo solo nel caso COOP
						if (isset($c['vtff_coop_codice']) && trim($c['vtff_coop_codice'])!='') {
							$valore = $campo['valueif'];
						}
					}
					
					if (strpos($campo['label'], "SETTORE MERCEOLOGICO") !== false) { // ------------------------------------------------------------ valorizzo in relazione al "tipo di riga" (GAS o LUCE)
						if ($notOfType == '_gas_') {
							$valore = 'ENERGIA ELETTRICA';//'LUCE';
						}
						else {
							$valore = 'GAS';
						}
					}
					
					
					if (strpos($campo['label'], "OFFERTA COMMERCIALE ".strtoupper($isOfType)) !== false) { // -------------------------------------- assegno il nome dell'offerta commerciale (in relazione al tipo di riga)
						$valore = $offerta_commerciale;
					}
					
					if (strpos($campo['label'], "OFFERTA_IMOLA") !== false) { // ------------------------------------------------------------------- assegno il valore in relazione al comune (filtro su elenco REF)
						$valore = 'no';
						if ($zona_offerta == 'IMOLA') {
							$valore = 'si';
						}
					}
					
					if (strpos($campo['label'], "HC MARCHE") !== false) { // ----------------------------------------------------------------------- assegno il valore in relazione al comune (filtro su elenco REF)
						$valore = 'no';
						if ($zona_offerta == 'MARCHE') {
							$valore = 'si';
						}
					}
					
					if (strpos($campo['label'], "INVIO ELETTRONICO DELLA BOLLETTA") !== false) { // ------------------------------------------------ gestisco i campi sull'invio elettronico della bolletta
						if (strpos($campo['label'], "INDIRIZZO MAIL") !== false) { // ............................................................... nel caso del campo email
							$valore = '';
							if ( $invio_elettronico_bolletta ) {
								$valore = $c['vtff_email'];
							}
						}
						else {	// ................................................................................................................. nel caso sia il campo relativo al fatto che sia abilitato o meno l'invio elettronico
							$valore = $c['vtff_opzione_natura'];
							if (strpos(strtoupper($offerta_commerciale), 'PFHN') !== false) { // anche per la Prezzo Fisso Hera Natura
								$valore = 'si';
							}
							if ($valore == 'si') {
								$invio_elettronico_bolletta = true;
							}
						}
					}
					
					$valore = iconv('CP1252','UTF-8', $valore);	// §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ normalizzo l'encoding
				}
				
				
				$excel
					->setCellValueExplicit(
						$colonna . $riga, 
						$valore, 
						PHPExcel_Cell_DataType::TYPE_STRING // forzo il tipo stringa per evitare che Excel trasformi il dato come vuole...
					);
					
				$colonna++;
			} // fine ciclo campi
			$riga++;
		
		} // fine ciclo doppiocampo
		
	} // fine ciclo compilazioni

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($save_dest);
	
} // fine ciclo sui blocchi di record da processare...


$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
/**/
echo '<hr/>';
echo '<a href="'.$save_dest.'">EXCEL</a><br/>';
echo '<hr/>';
/**/
echo 'Processati ' . $compilazioni_totali . ' record'.'<br/>';
echo 'Tempo generazione: ' . sprintf('%.4f',$callTime) . ' secondi'.'<br/>';
echo 'Memoria usata: '. convert(memory_get_usage(true)) .'<br/>';
echo 'Picco memoria: '. convert(memory_get_peak_usage(true)) .'<br/>';